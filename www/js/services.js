angular.module('starter.services', [])

.factory('Quest', function($http, $q, DB) {
    var deferred = $q.defer();
    var quest = {};
    // Some fake testing data
    data =  function(id){

        return $http.get("lib/buku/"+id+".json").success(function(response){

            var orderHeader = [];
            quest = response;


            var i = 1;
            angular.forEach(quest, function(value, key){
                orderHeader[i] = key;
                i++;

                var orderQuest = [];
                var j = 1;

                angular.forEach(value.quest, function(valueQuest, keyQuest){
                    orderQuest[j] = keyQuest;
                    j++;
                });

                angular.forEach(orderQuest, function(valueQuest,keyQuest){

                    if(orderQuest[keyQuest+1]){
                        quest[key].quest[valueQuest].next = orderQuest[keyQuest+1];
                    }else{
                        quest[key].quest[valueQuest].last = true;
                    }

                });

            });

            angular.forEach(orderHeader, function(value,key){
                if(orderHeader[key+1]==null){
                    quest[value].last = true;
                }else{
                    quest[value].next = orderHeader[key+1];
                }

            });

            return quest;
        });
    };

    getHint = function(id_survey, key, id_roster){
        if(id_roster){
             return DB.query('SELECT * FROM ms_result WHERE id_survey = ?  AND id_quest = ? AND id_crosstab = ?', [id_survey, key, id_roster])
            .then(function(result){
                return DB.fetchAll(result);
            });

        }else{
             return DB.query('SELECT * FROM ms_result WHERE id_survey = ?  AND id_quest = ?', [id_survey, key])
            .then(function(result){
                return DB.fetchAll(result);
            });

        }

      // return ;
    }
    getRoster = function(id_survey, id_buku, key_header) {
        return DB.query('SELECT * FROM ms_crosstab WHERE id_survey=? AND id_buku = ? AND key_header = ? ', [id_survey, id_buku, key_header])
        .then(function(result){

            return DB.fetchAll(result);
        });
    }
    getRosterStatus = function(id_survey, id_buku, key_header, value, key) {

        return DB.query('SELECT * FROM ms_result WHERE id_survey=? AND id_buku = ? AND key_header = ? AND id_crosstab = ? AND is_last = 1', [id_survey, id_buku, key_header, key])
        .then(function(result){

            return DB.fetchAll(result);
        });
    }
    saveHistory = function(scope){

        if(scope.id_roster!=''){
            DB.query("DELETE FROM ms_quest_history WHERE id_survey = ? AND id_header = ? AND id_quest = ? AND id_crosstab = ?",[scope.id_survey, scope.key_header, scope.id_quest, scope.id_roster]);
            DB.query("DELETE FROM ms_quest_history WHERE id_survey = ? AND next_header = ? AND next_quest = ? AND id_crosstab = ?",[scope.id_survey, scope.nextHeader, scope.nextQuest, scope.id_roster]);
            query = DB.query("INSERT INTO ms_quest_history (id_survey,id_header,id_quest, next_header, next_quest, id_crosstab) VALUES (?,?,?,?,?,?)",[scope.id_survey, scope.key_header, scope.id_quest, scope.nextHeader, scope.nextQuest, scope.id_roster]);

        }else{
            DB.query("DELETE FROM ms_quest_history WHERE id_survey = ? AND id_header = ? AND id_quest = ?",[scope.id_survey, scope.key_header, scope.id_quest]);
            DB.query("DELETE FROM ms_quest_history WHERE id_survey = ? AND next_header = ? AND next_quest = ?",[scope.id_survey, scope.nextHeader, scope.nextQuest]);
            query = DB.query("INSERT INTO ms_quest_history (id_survey,id_header,id_quest, next_header, next_quest) VALUES (?,?,?,?,?)",[scope.id_survey, scope.key_header, scope.id_quest, scope.nextHeader, scope.nextQuest]);

        }

    }
    getHistory = function(scope){

        return DB.query("SELECT * FROM ms_quest_history WHERE id_survey = ? AND next_header = ? AND next_quest = ?",[scope.id_survey, scope.key_header, scope.id_quest]).then(function(result){

            return DB.fetch(result);
        });

    }
    return {
        data : data,
        getHint : getHint,
        getRoster : getRoster,
        getRosterStatus : getRosterStatus,
        saveHistory : saveHistory,
        getHistory : getHistory
    };
    /*data: function() {
            var quest = deferred.promise.value;


        }*/
}).factory('Book', function($http, $q) {
    var deferred = $q.defer();
    var book = {};
    // Some fake testing data
    data =  $http.get("lib/buku.json").success(function(response){
      return book;
    });


      // return ;
    return {
        data : data
    };
    /*data: function() {
            var quest = deferred.promise.value;


        }*/
}).factory('DownloadBook', function($http, $q) {

    var deferred = $q.defer();
    var book = {};
    // Some fake testing data
    data =  $http.get("lib/buku.json").success(function(response){
      return book;
    });


      // return ;
    return {
        data : data
    };
    /*data: function() {
            var quest = deferred.promise.value;


        }*/
}).factory('DB', function($q, DB_CONFIG) {
    var self = this;
     self.db = window.openDatabase(DB_CONFIG.name, '1.0', 'database', -1);

    self.init = function() {
        // Use self.db = window.sqlitePlugin.openDatabase({name: DB_CONFIG.name}); in production

        self.db = window.openDatabase(DB_CONFIG.name, '1.0', 'database', -1);

        angular.forEach(DB_CONFIG.tables, function(table) {
            var columns = [];

            angular.forEach(table.columns, function(column) {
                columns.push(column.name + ' ' + column.type);
            });

            var query = 'CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + columns.join(',') + ')';
            self.query(query);
        });
    };

    self.query = function(query, bindings) {
        bindings = typeof bindings !== 'undefined' ? bindings : [];
        var deferred = $q.defer();

        self.db.transaction(function(transaction) {

            transaction.executeSql(query, bindings, function(transaction, result) {

                deferred.resolve(result);

            }, function(transaction, error) {

                deferred.reject(error);
            });
        });

        return deferred.promise;
    };

    self.fetchAll = function(result) {
        var output = [];

        for (var i = 0; i < result.rows.length; i++) {
            output.push(result.rows.item(i));
        }

        return output;
    };

    self.fetch = function(result) {
        if(result.rows.length > 0){
            return result.rows.item(0);
        }

    };

    return self;
})
// Resource service example
.factory('Answer', function(DB) {
    var self = this;

    self.all = function() {
        return DB.query('SELECT * FROM documents')
        .then(function(result){
            return DB.fetchAll(result);
        });
    };

    self.getById = function(id) {
        return DB.query('SELECT * FROM documents WHERE id = ?', [id])
        .then(function(result){
            return DB.fetch(result);
        });
    };
    self.getByIdQuestion = function(id_survey, id_header, id_quest, key, id_crosstab, quest) {
            if(id_crosstab!=''){
                return DB.query('SELECT * FROM ms_result WHERE id_survey = ?  AND id_header = ? AND id_quest = ? AND key = ? AND id_crosstab = ?', [id_survey, id_header, id_quest, key, id_crosstab])
                .then(function(result){

                    if(quest.content[key].replicate > 1){
                         return DB.fetchAll(result);
                    }else{
                         return DB.fetch(result);
                    }
                });

            }else{
                 return DB.query('SELECT * FROM ms_result WHERE id_survey = ?  AND id_header = ? AND id_quest = ? AND key = ?', [id_survey, id_header, id_quest, key])
                .then(function(result){

                    if(quest.content[key].replicate!=null && quest.content[key].replicate>0){
                        return DB.fetchAll(result);
                    }else if(result.rows.length > 1){

                         return DB.fetchAll(result);
                    }else{
                         return DB.fetch(result);

                    }
                });
            }
    };
    self.getByAnswer = function(id_survey, key, id_crosstab, quest) {
        if(id_crosstab!='' && typeof id_crosstab != "undefined"){
            return DB.query('SELECT * FROM ms_result WHERE id_survey = ?  AND IN(' + key+ ') AND id_crosstab = ?', [id_survey, id_crosstab])
            .then(function(result){

                if(quest.replicate > 1){
                     return DB.fetchAll(result);
                }else{
                     return DB.fetch(result);
                }
            });
        }else{
             return DB.query('SELECT * FROM ms_result WHERE id_survey = ?  AND key IN(' + key + ')', [id_survey])
             .then(function(result){

                if(result.rows.length > 1){
                     return DB.fetchAll(result);
                }else{
                     return DB.fetch(result);
                }
            });
        }
    };
    self.getByQuestion = function(id_survey, key, id_crosstab) {
        if(id_crosstab!='' && typeof id_crosstab != "undefined"){
            return DB.query('SELECT * FROM ms_result WHERE id_survey = ? AND id_quest = ? AND id_crosstab = ?', [id_survey, key, id_crosstab])
            .then(function(result){

                if(quest.replicate > 1){
                     return DB.fetchAll(result);
                }else{
                     return DB.fetch(result);
                }
            });
        }else{
             return DB.query('SELECT * FROM ms_result WHERE id_survey = ? AND id_quest = ?', [id_survey, key])
             .then(function(result){
                return DB.fetchAll(result);
            });

        }
    };


    return self;
}).factory('Master', function(DB, $http) {
    var self = this;
    self.remark = function(id_survey, id_buku){
         return DB.query('SELECT * FROM ms_remark WHERE id_survey = ? AND id_buku = ?', [id_survey, id_buku])
        .then(function(result){
            return DB.fetchAll(result);
        });
    }
    self.all = function(id_buku,id_ea) {
        return DB.query('SELECT * FROM ms_master WHERE  id_buku = ? AND id_ea = ?',[ id_buku, id_ea])
        .then(function(result){
            return DB.fetchAll(result);
        });
    };

    self.getById = function(id) {
        return DB.query('SELECT * FROM ms_master WHERE id = ?', [id])
        .then(function(result){

            return DB.fetch(result);
        });
    };
    self.getMaster = function() {
        return DB.query('SELECT * FROM ms_master ', [])
        .then(function(result){
            return DB.fetch(result);
        });
    };

    self.getEa =  function(id){

        return $http.get("lib/lokasi.json").success(function(response){
           return response;
        });
    };

    self.enumerator =  function(id){
        var ea = $.jStorage.get('ea');
        if(parseInt(ea) >= 300){
            var json = 'enumerator.json';
        }else{
            var json = 'enumerator_field.json';
        }
        return $http.get("lib/"+json).success(function(response){


            return response.data;
        });
    };

    self.getPerangkat =  function(ea, key){
        return $http.get("lib/perangkat.json").success(function(response){
           return response;
        });
    };


    return self;
}).factory('Record', function($rootScope, $state){
    var self = this;
    var mediaRecorder = null;

//    var mediaRecorder = (typeof Media =='undefined') ? false :

    self.createFile = function(nama_file){
     nama_file = nama_file + '.amr';
      window.resolveLocalFileSystemURL(cordova.file.externalRootDirectory, function (dirEntry) {
          console.log(dirEntry);
          var isAppend = true;
                      createFile(dirEntry, nama_file, true);
                      function createFile(dirEntry, fileName, isAppend) {
                           // Creates a new file or returns the file if it already exists.
                           dirEntry.getFile(nama_file, {create: true, exclusive: false}, function(fileEntry) {

                               writeFile(fileEntry, '', isAppend);

                           }, function(error){
                               console.log(error)
                           });

                       }
                       function writeFile(fileEntry, dataObj) {
                          // Create a FileWriter object for our FileEntry (log.txt).
                          fileEntry.createWriter(function (fileWriter) {

                              fileWriter.onwriteend = function() {
                                  console.log("Successful file write...");

                              };

                              fileWriter.onerror = function (e) {
                                  console.log("Failed file write: " + e.toString());
                              };


                              fileWriter.write(dataObj);

                          });
                      }
        console.log(dirEntry);
      }, function (error) {
        console.error('Error'+error)
        console.error(error)
      });
    }

    self.record = function(nama_file){
    nama_file = nama_file + '.mp3';
    self.mediaRecorder = new Media(cordova.file.externalRootDirectory+nama_file, function(mediaRec){
    console.log(mediaRec)
                                 mediaRec.release();
                             }, function(msg){
                                 console.log('Message')
                                 console.log(msg)
                             }, function (status) {
                                              console.error('Status:'),
                                              console.log(status)
                                            });
        self.mediaRecorder.startRecord();
    }
    self.stop = function(is_stop){
        console.log('stop')
        if(is_stop){
          self.mediaRecorder.stopRecord();
        }

    }
    return self;
});
