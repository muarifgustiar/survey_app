angular.module('starter.controllers', [])

.controller('LoginCtrl', function($scope, $http, $location, $state) {
    $scope.login = function(){
        $.jStorage.set('kode', $scope.kode.toUpperCase());
        $.jStorage.set('ea', $scope.ea.toUpperCase());
        if($scope.loginForm.$valid){
            $state.go("survey.dash");
        }else{
            alert('Masukkan data dengan benar!');
        }

    }
})
.controller('DashCtrl', function($scope, $http, $location, $state) {
    $scope.logout = function(){
        $.jStorage.deleteKey('kode');
        $.jStorage.deleteKey('ea');
         $state.go("survey.login");
    }
})
.controller('QuestCtrl', function($scope, $state, $stateParams, Quest, $q, $http,$rootScope,DB, $ionicPopup, $ionicModal, $compile, Answer,$ionicLoading, Record) {


    $scope.data = {};
    $scope.id_buku = $stateParams.id_buku;
    $scope.key_header = $stateParams.id_header;
    $scope.id_quest = $stateParams.id_quest;
    $scope.id_roster = $stateParams.id_roster;
    $scope.answer  = '';
    $scope.crosstab = {};

    $scope.hint  = '';
    $scope.roster  = '';
    $scope.isinsert = false;
    $scope.insertOther = false;
    $scope.id_survey = $stateParams.id_survey;
    $scope.kode_petugas = $.jStorage.get('kode');
    $scope.ea = $.jStorage.get('ea');
    $scope.surveyForm = {};
    $scope.is_valid = true;

    $scope.init = function(){
        $ionicLoading.show();
        if(typeof $scope.id_buku!=''){

            Quest.data($scope.id_buku).then(function(result){

                $scope.data = result.data;
                $scope.header = $scope.data[$scope.key_header];
                if(typeof $scope.header.crosstab!='undefined'){
                    initCrosstab = Object.keys($scope.header.crosstab).length;
                }

                $scope.getRoster( );

                if($scope.header.is_crosstab == true && $scope.header.is_custom==true){

                    $scope.insertOther = true;
                }
                $scope.id_header = $scope.header.id;

                $scope.quest = $scope.data[$scope.key_header].quest[$scope.id_quest];

                if(typeof $scope.header.crosstab != 'undefined' && $state.current.name=="survey.quests" && typeof $scope.header.crosstab[$scope.id_roster] !='undefined'){

                    $scope.quest.label = $scope.quest.label.toString().replace("{}",$scope.header.crosstab[$scope.id_roster].nama);

                }


                $scope.getHint = function(){

                    if(typeof $scope.quest!='undefined'){

                        Quest.getHint( $scope.id_survey, $scope.quest.hint, $scope.id_roster).then(function(result){


                            angular.forEach(result, function(value,key){
                                console.log( $scope.data[value.key_header])
                                var dataHint = $scope.data[value.key_header].quest[$scope.quest.hint];

                                if(dataHint.content[value.key].type=="checkbox"){
                                      $scope.hint = dataHint.content[value.key].label;
                                }else if(dataHint.content[value.key].type=="text"){
                                      $scope.hint = value.value;
                                }else if(dataHint.content[value.key].type=="other"){

                                      $scope.hint = dataHint.content[value.key].label +' '+value.value_2;
                                }

                                if($scope.quest.hint){

                                    $scope.quest.label = $scope.quest.label.toString().replace("{hint}",$scope.hint);
                                }

                            })


                        });
                    }
                }

                $scope.getHint();

                if($scope.header.last && $scope.quest.next_to =='finish'){
                    $scope.end = true;
                }else{
                     $scope.end = false;
                }


                if(typeof $scope.quest!='undefined'){

                    if($scope.header.next!=''&&$scope.quest.last==true){

                        $scope.nextHeader = $scope.header.next;

                        $scope.nextQuest = Object.keys($scope.data[$scope.header.next].quest)[0];

                    }

                    if($scope.quest.next>0&&$scope.header.next!=''){
                        $scope.nextQuest = $scope.quest.next;
                        $scope.nextHeader = $scope.key_header;

                    }

                    $scope.is_roster($scope.nextHeader);

                    if($scope.quest.next_to == 'finish' || $scope.quest.last ){
                         $scope.nextHeader = $scope.header.next;
                        $scope.finish();


                    }
                }

                if($state.current.name=="survey.roster"){
                    $scope.nextHeader = $scope.header.next;
                    $scope.is_roster($scope.nextHeader);
                    $scope.nextQuest = Object.keys($scope.data[$scope.header.next].quest)[0];
                }

                angular.forEach($scope.quest.content, function(value,key){

                    Answer.getByIdQuestion($scope.id_survey, $scope.id_header, $scope.id_quest, key, $scope.id_roster, $scope.quest).then(function(result){

                        if(typeof result !='undefined'){
                            if(value.type=="checkbox"){
                                $scope.quest.content[result.key].value = (result.value === 'true');

                            }
                            else if(value.type=="other"){

                                $scope.quest.content[result.key].value = (result.value === 'true');
                                $scope.quest.content[result.key].value_2 = result.value_2;
                                $scope.quest.content[result.key].value_3 = result.value_3;
                            }else{

                                if(value.replicate > 0){

                                    value.value = [];
                                    value.value_2 = [];

                                    angular.forEach(result, function(value, key){
                                        $scope.quest.content[value.key].value[value.key_multiple] = value.value;
                                        $scope.quest.content[value.key].value_2[value.key_multiple] = value.value_2;
                                    })


                                }else{
                                    if(typeof $scope.quest.content[result.key]!='undefined'){
                                        if( result.value_2!="" && typeof  result.value_2!="undefined"){
                                            $scope.quest.content[result.key].value_2 = result.value_2;
                                        }
                                        $scope.quest.content[result.key].value = result.value;
                                    }


                                }

                            }
                        }
                        $ionicLoading.hide();
                    });
                });

                if(Object.keys($scope.data[$scope.key_header].quest)[0]==$scope.id_quest){
                    $scope.is_first_quest = true;
                }

            });
            Quest.getHistory($scope).then(function(result){
                    if(typeof result!='undefined' ){

                        $scope.prevHeader = result.id_header;
                        $scope.prevQuest = result.id_quest;

                        $scope.check_roster_before(result.id_header);

                    }else{

                        if($scope.id_roster!=''){
                             $scope.check_roster_before($scope.key_header);
                        }else{
                              $scope.check_roster_before(($scope.key_header - 1));
                        }


                    }



            });

        }

    }
    $scope.$watch('data', function(newValue, oldValue){
        Quest.getHistory($scope).then(function(result){

            if(typeof result!='undefined'){
                $scope.check_roster_before(result.id_header);

            }else{
               if($scope.id_roster!=''){
                     $scope.check_roster_before($scope.key_header);
                }else{
                      $scope.check_roster_before(($scope.key_header - 1));
                }
            }

        });
    })
   
        $ionicModal.fromTemplateUrl('my-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
          }).then(function(modal) {
            $scope.modalGoTo = modal;
          });
    
    $scope.showGoTo = function(){
       $scope.modalGoTo.show();
    }
    $scope.closeGoTo = function(){
       $scope.modalGoTo.hide();
    }
    $scope.goTo = function(id_header){
        console.log($scope)
       $scope.modalGoTo.hide();
        if($scope.data[id_header].is_crosstab){
            $state.go("survey.roster", {
                id_buku: $scope.id_buku,
                id_survey: $stateParams.id_survey,
                id_header: id_header,
                id_quest: Object.keys($scope.data[id_header].quest)[0]
            });
        }else{
            $state.go("survey.quests", {
                id_buku: $scope.id_buku,
                id_survey: $stateParams.id_survey,
                id_header: id_header,
                id_quest: Object.keys($scope.data[id_header].quest)[0],
                id_roster:''
            });
        }
    }
    $scope.check_roster = function(){

        if($scope.header.crosstab){
            $scope.is_valid = false;
        }
        angular.forEach($scope.header.crosstab, function(value, key){
            if(value.status=="0" || value.status=="1"){
                $scope.is_valid = false;
                return false;
            }else{
                $scope.is_valid = true;
            }
        });

    }
    $scope.getRoster = function(){


        Quest.getRoster($scope.id_survey,$scope.id_buku, $scope.key_header).then(function(result){

            if(typeof $scope.header.crosstab!='undefined'){

                angular.forEach(result, function(value, key){

                    if($scope.header.is_empty==false){
                        key = Object.keys($scope.header.crosstab)[Object.keys($scope.header.crosstab).length - 1];
                        p_key = parseInt(key) + 1;
                        $scope.header.crosstab[p_key.toString()] = {"nama":value.value, "code":value.code};
                        Quest.getRosterStatus($scope.id_survey,$scope.id_buku, $scope.key_header, value, key).then(function(result){

                        })
                    }else{
                        $scope.header.crosstab[value.code.toString()] = {"nama":value.value, "code":value.code, "status":value.status};
                    }

                })


                if(typeof $scope.header.crosstab != 'undefined' && $state.current.name=="survey.quests" && typeof $scope.header.crosstab[$scope.id_roster] !='undefined'){

                        $scope.quest.label = $scope.quest.label.toString().replace("{}",$scope.header.crosstab[$scope.id_roster].nama);
                    }
                if($scope.header.is_crosstab == true && Object.keys($scope.header.crosstab).length > 0 && $scope.header.is_custom == true && result.length > 0){
                     $scope.insertOther = false;
                }
                if($scope.header.is_empty){
                    $scope.insertOther = true;
                }
            }
             if(!$scope.header.is_empty){

                angular.forEach($scope.header.crosstab, function(value, key){

                    Quest.getRosterStatus($scope.id_survey,$scope.id_buku, $scope.key_header, value, key).then(function(result){

                        if($scope.header.crosstab[key].status!=1||result.length>0){
                            if(result.length){
                                $scope.header.crosstab[key].status = 2;

                                $scope.header.crosstab[parseInt(key)+1].status = 1;
                                $scope.is_valid = true;
                            }else if(key==0){
                                $scope.header.crosstab[key].status = 1;
                            }else{
                                 $scope.header.crosstab[key].status = 0;
                                 $scope.is_valid = false;
                            }
                        }

                    })
                })
            }
             $scope.check_roster();
             $ionicLoading.hide();
        })




    }
    $scope.saveRoster = function(){
        var status = 0;
       if($scope.header.is_empty==true){
            code = Object.keys($scope.header.crosstab).length + 1;
            p_key = code;
            DB.query("SELECT COUNT(*) FROM ms_crosstab WHERE id_survey = ? AND id_header = ? AND id_survey = ?");
            if(code==1 ){
                status = 1;
                status = status.toString();
            }else{
                status = 0;
                status = status.toString();
            }

        }
        else{

            DB.query("DELETE FROM ms_crosstab WHERE id_survey=? AND key_header = ? AND id_buku = ?",[$scope.id_survey,$scope.key_header, $scope.id_buku]);

            code = 'v';
            key = Object.keys($scope.header.crosstab)[Object.keys($scope.header.crosstab).length - 1];

            p_key = parseInt(key) + 1;

        }

        query = DB.query("INSERT INTO ms_crosstab(id_survey, kode_petugas, id_buku, key_header, id_header, value,code,status, entry_stamp) VALUES(?,?,?,?,?,?,?,?,?)",
                    [$scope.id_survey, $scope.kode_petugas, $scope.id_buku, $scope.key_header,$scope.id_header, $scope.crosstab.value.toUpperCase(), code.toString().toUpperCase(),status, moment().format('YYYY-MM-DD HH:mm:ss')]
                    );


        $scope.header.crosstab[p_key] = {"nama":$scope.crosstab.value.toUpperCase(), "code":code,"status":status};
        if($scope.header.is_crosstab == true && Object.keys($scope.header.crosstab).length > 0 && $scope.header.is_custom == true){
            $scope.insertOther = false;
        }

        if($scope.header.is_crosstab == true && $scope.header.is_empty==true && $scope.header.is_custom == true){
            $scope.insertOther = true;
            $scope.saveOther = false;
            $scope.crosstab.value = '';
        }

        $scope.displayForm = false;
        $scope.check_roster();
    }

    $scope.create_remark = function(key,data){
        $scope._data = {};
          var myPopup = $ionicPopup.show({
            template: '<textarea ng-model="_data.remark" rows="5"></textarea>',
            title: 'Catatan Pewawancara',
            scope: $scope,
            buttons: [
              { text: 'Cancel' },
              {
                text: '<b>Save</b>',
                type: 'button-positive',
                onTap: function(e) {

                    query = DB.query('INSERT INTO ms_remark(id_survey,id_buku,  id_quest, variable, remark) VALUES (?,?,?,?,?)',[$scope.id_survey, $scope.id_buku, $scope.id_quest, $scope.quest.code,$scope._data.remark ]);

                }
              }
            ]
          });
    }
    $scope.edit_roster = function(key,data){
        $scope._data = {};
          var myPopup = $ionicPopup.show({
            template: '<input type="text" ng-model="_data.newValue">',
            title: 'Edit',
            scope: $scope,
            buttons: [
              { text: 'Cancel' },
              {
                text: '<b>Save</b>',
                type: 'button-positive',
                onTap: function(e) {
                    data.nama = $scope._data.newValue;
                    query = DB.query('UPDATE ms_crosstab SET value = ? WHERE code = ? AND id_survey = ?',[$scope._data.newValue, data.code, $scope.id_survey]);

                }
              }
            ]
          });
    }
    $scope.delete_roster = function(key,data){
        $scope._data = {};
          var myPopup = $ionicPopup.show({
            title: 'Apakah benar akan menghapus data?',
            scope: $scope,
            buttons: [
              { text: 'Cancel' },
              {
                text: '<b>Hapus</b>',
                type: 'button-assertive',
                onTap: function(e) {

                    query = DB.query('DELETE FROM ms_crosstab WHERE code = ? AND id_survey = ?',[data.code, $scope.id_survey]);
                     delete $scope.header.crosstab[key];
                }
              }
            ]
          });
    }

    $scope.insert = function(){
        $scope.displayForm = true;
        $scope.insertOther = false;
        $scope.saveOther = true;
    }
    $scope.is_roster = function(next){
        // if(typeof $scope.data[next]!= 'undefined'){
            if($scope.data[next].is_crosstab==true && $scope.id_roster == ''){

                $scope.roster = true;
            }else{
                $scope.roster = false;
            }
        // }

    }

    $scope.check_roster_before = function(before){

        if((typeof $scope.data[before]!= 'undefined' && $scope.data[before].is_crosstab==true && $scope.id_roster =='') || ($scope.header.is_crosstab && typeof $scope.quest.first != 'undefined' && $scope.id_roster !='')){
            if(!($scope.key_header==1 && $scope.quest.first==true)){
                 $scope.is_roster_before = true;
            }

            if($scope.header.is_crosstab && typeof $scope.quest.first != 'undefined' && $scope.id_roster !=''){

                $scope.prevHeader = $scope.key_header;
                $scope.prevQuest = Object.keys($scope.data[$scope.key_header].quest)[0];
            }else{
                $scope.prevHeader = before;
                $scope.prevQuest = Object.keys($scope.data[before].quest)[0];
            }

        }else{
            $scope.is_roster_before = false;
        }

    }

    $scope.saveParsial = function(){
       if(typeof Media !='undefined'){
            Record.stop(true);
        }

        if( $scope.id_roster!=''){
            query = DB.query('UPDATE ms_master SET id_last_header = ?, id_key_header = ?, id_last_quest =?, id_last_roster=? WHERE id = ?',
                [$scope.id_header, $scope.key_header, $scope.id_quest, $scope.id_roster, $scope.id_survey]);
        }else{
            query = DB.query('UPDATE ms_master SET id_last_header = ?, id_key_header = ?, id_last_quest =?,  WHERE id = ?',
                [$scope.id_header, $scope.key_header, $scope.id_quest,  $scope.id_survey]);
        }
        query = DB.query('UPDATE ms_master_history SET status = 2, time_end = ? WHERE id_survey = ? AND (time_end IS NULL) OR (status IS NULL)',[moment().format('HH:mm:ss'), $scope.id_survey]);

        $state.go("survey.start", {
            id_buku: $scope.id_buku
        });
    }
    $scope.finish = function(){

        if($scope.quest.last ||$scope.quest.next_to == 'finish'){

            $scope.is_roster($scope.nextHeader);
            if($scope.data[$scope.header.next].is_crosstab){
                $scope.roster = true;
            }

        }

        $scope.nextHeader = $scope.header.next;
        $scope.nextQuest = Object.keys($scope.data[$scope.header.next].quest)[0];
        if(($scope.quest.last==true||$scope.quest.next_to =='finish') && $scope.id_roster!='' ){

            $scope.nextHeader = $scope.key_header;
             $scope.nextQuest = Object.keys($scope.data[$scope.key_header].quest)[0];
            $scope.roster = true;
        }



    }
    $scope.save = function(){
        if(typeof $scope.id_roster == 'undefined' || $scope.id_roster =='' || $scope.is_crosstab ==false ){
            query = DB.query("DELETE FROM ms_result WHERE id_survey=? AND key_header = ? AND id_header = ? AND id_quest = ?",[$scope.id_survey,$scope.key_header, $scope.id_header, $scope.id_quest]);

            angular.forEach($scope.quest.content, function(value, key){


                if(value.replicate>0 || parseInt(value.replicate)>0){

                    angular.forEach(value.value, function(value_multiple, key_multiple){

                        query = DB.query("INSERT INTO ms_result(id_survey, kode_petugas, id_buku, key_header, id_header,id_quest,key, key_multiple, id_crosstab,value,value_2, entry_stamp) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)",
                        [$scope.id_survey, $scope.kode_petugas, $scope.id_buku, $scope.key_header,$scope.id_header, $scope.id_quest, key, key_multiple, null, value_multiple, value.value_2[key_multiple], moment().format('YYYY-MM-DD HH:mm:ss')]
                        );

                    });

                    if(value.value && parseInt(value.replicate)>0 && value.type == 'other'){

                        angular.forEach(value.value_2, function(value_multiple, key_multiple){

                            query = DB.query("INSERT INTO ms_result(id_survey, kode_petugas, id_buku, key_header, id_header,id_quest,key, key_multiple, id_crosstab,value,value_2, entry_stamp) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)",
                            [$scope.id_survey, $scope.kode_petugas, $scope.id_buku, $scope.key_header,$scope.id_header, $scope.id_quest, key, key_multiple, null, true, value_multiple, moment().format('YYYY-MM-DD HH:mm:ss')]
                            );

                        });

                    }

                }else{

                    if(((value.value==true ||value.value=='true') && value.type=="checkbox")||(value.type=="text" && value.type!='')){
                        query = DB.query("INSERT INTO ms_result(id_survey, kode_petugas, id_buku, key_header, id_header,id_quest,key,id_crosstab,value,value_2,  entry_stamp) VALUES(?,?,?,?,?,?,?,?,?,?,?)",
                        [$scope.id_survey, $scope.kode_petugas, $scope.id_buku, $scope.key_header,$scope.id_header, $scope.id_quest, key, null, value.value, value.value_2, moment().format('YYYY-MM-DD HH:mm:ss')]
                        );

                    }
                    if(value.type=="other" && value.type!='' && value.value==true){

                        query =  DB.query("INSERT INTO ms_result(id_survey, kode_petugas, id_buku, key_header, id_header,id_quest,key,id_crosstab,value,value_2, value_3, entry_stamp) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)",
                        [$scope.id_survey, $scope.kode_petugas, $scope.id_buku, $scope.key_header,$scope.id_header, $scope.id_quest, key, null, value.value, value.value_2, value.value_3, moment().format('YYYY-MM-DD HH:mm:ss')]
                        );
                    }


                }


            });
        }else{

            query = DB.query("DELETE FROM ms_result WHERE id_survey=? AND key_header = ? AND id_header = ? AND id_quest = ? AND id_crosstab = ?",[$scope.id_survey,$scope.key_header, $scope.id_header, $scope.id_quest, $scope.id_roster]);
            angular.forEach($scope.quest.content, function(value, key){

                if(((value.value==true ||value.value=='true') && value.type=="checkbox")||(value.type=="text" && value.type!='')){
                    query = DB.query("INSERT INTO ms_result(id_survey, kode_petugas, id_buku, key_header, id_header,id_quest,key,id_crosstab,value, entry_stamp) VALUES(?,?,?,?,?,?,?,?,?,?)",
                    [$scope.id_survey, $scope.kode_petugas, $scope.id_buku, $scope.key_header,$scope.id_header, $scope.id_quest, key, $scope.id_roster, value.value, moment().format('YYYY-MM-DD HH:mm:ss')]
                    );
                }

                if(value.type=="other" && value.type!='' && value.value==true){
                    query =  DB.query("INSERT INTO ms_result(id_survey, kode_petugas, id_buku, key_header, id_header,id_quest,key,id_crosstab,value,value_2, value_3, entry_stamp) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)",
                    [$scope.id_survey, $scope.kode_petugas, $scope.id_buku, $scope.key_header,$scope.id_header, $scope.id_quest, key, $scope.id_roster, value.value, value.value_2, value.value_3,moment().format('YYYY-MM-DD HH:mm:ss')]
                    );
                }

                if($scope.quest.content[key].next_to=='finish' || $scope.quest.last){

                    query = DB.query("UPDATE ms_crosstab SET status = ? WHERE id_survey=? AND code = ? AND id_buku = ? AND id_header = ?",["2", $scope.id_survey,$scope.id_roster, $scope.id_buku, $scope.id_header]);

                    next = DB.query("UPDATE ms_crosstab SET status = ? WHERE id_survey=? AND code = ? AND id_buku = ? AND id_header = ?",["1", $scope.id_survey,(parseInt($scope.id_roster) + 1).toString(), $scope.id_buku, $scope.id_header]);
                     query = DB.query('UPDATE ms_result SET is_last = 1 WHERE id_survey=? AND id_buku = ? AND id_header = ? AND key_header = ? AND id_quest = ? AND id_crosstab = ?',[$scope.id_survey, $scope.id_buku, $scope.id_header, $scope.key_header, $scope.id_quest, $scope.id_roster]);

                }
            });

        }

        if($scope.header.last && $scope.quest.next_to =='finish'){
            $scope.end = true;
        }else{
             $scope.end = false;
        }

        if(!(($scope.quest.last || $scope.quest.next_to=="finish")  && $scope.id_roster!='')){
            Quest.saveHistory($scope);
        }else{
            query = DB.query('UPDATE ms_result SET is_last = 1 WHERE id_survey=? AND id_buku = ? AND id_header = ? AND key_header = ? AND id_quest = ? AND id_crosstab = ?',[$scope.id_survey, $scope.id_buku, $scope.id_header, $scope.key_header, $scope.id_quest, $scope.id_roster]);

        }


    }
    $scope.init();
    $scope.remove = function(chat) {
        Quest.remove(chat);
    };
})

.controller('MasterCtrl', function($scope, $stateParams, DB, $state, Master, Book, $q, $http,$rootScope,$ionicPopup, Quest, Record) {

    $scope.survey = {};
    $scope.id_buku = $stateParams.id_buku;
    $scope.id_survey = $stateParams.id_survey;
    $scope.kode_petugas = $.jStorage.get('kode');
    $scope.nama_petugas = "";
    $scope.kode_ea = $.jStorage.get('ea');
    $scope.enumerator = {};
    $scope.data_survey = {
        no : '',
        id_art : '',
        nama : '',
        spv : '',
        dusun : '',
        rw : '',
        rt: '',
        address: '',
        location:'',
        is_telp:false,
        phone:'',
        is_hp:false,
        hp:'',
        tt:false
    };



    Master.all($scope.id_buku, $scope.kode_ea).then(function(result){

        $scope.survey = result;

    });
    Master.enumerator($scope.kode_petugas).then(function(result){

        var _return = {};
        angular.forEach(result.data, function(value, key){
            var spv = Object.keys(value)[0];
            angular.forEach(value, function(values, keys){
                _return[keys] = {};
                _return[keys]['nama'] = values;
                _return[keys]['spv'] = spv;
            });
        })

        $scope.enumerator = _return;
        $scope.nama_petugas = $scope.enumerator[$scope.kode_petugas].nama;
        $scope.nama_spv = $scope.enumerator[$scope.enumerator[$scope.kode_petugas].spv].nama;
        $scope.kode_spv = $scope.enumerator[$scope.enumerator[$scope.kode_petugas].spv].spv;

    });

    Master.getById($scope.id_survey).then(function(result){
        $scope.data_survey.no = result.no;
        $scope.data_survey.id_art = result.id_art;
        $scope.data_survey.nama = result.nama;
        $scope.data_survey.spv = result.spv;
        $scope.data_survey.dusun = result.dusun;
        $scope.data_survey.rw = result.rw;
        $scope.data_survey.rt = result.rt;
        $scope.data_survey.address = result.address;
        $scope.data_survey.location = result.location;
        $scope.data_survey.is_telp = (result.is_telp === 'true');
        $scope.data_survey.phone = result.phone;
        $scope.data_survey.is_hp = (result.is_hp === 'true');
        $scope.data_survey.owner = result.owner;
        $scope.data_survey.hp = result.hp;
        $scope.data_survey.tt = (result.tt === 'true');


    });
     Master.getEa().then(function(response){
        $scope.ea = response.data[$scope.kode_ea];
    })
    Quest.data($scope.id_buku).then(function(result){
        $scope.q = result.data;

    });
    Book.data.then(function(response){
        $scope.buku = response.data;
    })

    $scope.emptyPhone = function(data){
        data.phone = '';
        data.hp = '';
        data.owner = '';
        data.is_hp = false;
        data.is_telp = false;
    }
    $scope.emptyTT = function(data){
        data.tt = '';
    }
    $scope.saveConsent = function(data){

        query = DB.query("INSERT INTO ms_master(id_buku,id_enumerator, id_ea, tt, answer,remark,entry_stamp) VALUES(?,?,?,?,?,?,?)",
            [$scope.id_buku, $scope.kode_petugas, $scope.kode_ea, data.tt, data.answer, data.remark,  moment().format('YYYY-MM-DD HH:mm:ss')]
        ).then(function(result){

            if(data.answer==1){
                query = DB.query("INSERT INTO ms_master_history(id_survey, id_buku,visit, date, time_start, time_end, status, remark) VALUES(?,?,?,?,?,?,?,?)",
            [result.insertId, $scope.id_buku, 1, moment().format('YYYY-MM-DD'),moment().format('HH:mm:ss'),null, null, null]);
                ;
                $state.go("survey.master", {
                    id_survey: result.insertId,
                    id_buku: $scope.id_buku
                });
            }else{
                query = DB.query("INSERT INTO ms_master_history(id_survey, id_buku,visit, date, time_start, time_end, status, remark) VALUES(?,?,?,?,?,?,?,?)",
            [result.insertId, $scope.id_buku, 1, moment().format('YYYY-MM-DD'),moment().format('HH:mm:ss'),moment().format('HH:mm:ss'), 3, data.remark]);
                $state.go("survey.remark", {
                    id_survey: result.insertId,
                    id_buku: $scope.id_buku
                });
            }
        });
    }
    $scope.continue_survey = function(id_survey, id_buku){
        query = DB.query('SELECT * FROM ms_master_history WHERE id_survey = ? AND id_buku = ? ORDER BY id desc',[id_survey, id_buku]).then(function(result){
            var res = DB.fetch(result);

              query = DB.query("INSERT INTO ms_master_history(id_survey, id_buku,visit, date, time_start) VALUES(?,?,?,?,?)",
            [id_survey, id_buku, parseInt(res.visit) + 1, moment().format('YYYY-MM-DD'),moment().format('HH:mm:ss')]);
        });


    }
    $scope.save = function(data){

        var kode_buku = "";
        switch($scope.id_buku){
            case '4':
            case '5':
            case '2':
            case '8':
                kode_buku = $scope.buku[$scope.id_buku].code + $scope.kode_ea;
                break;
            case '7':
            case '6':
            case '1':
            case '3':
                kode_buku = $scope.buku[$scope.id_buku].code + $scope.kode_ea + data.no;
                break;
            case '9':
                kode_buku = $scope.buku[$scope.id_buku].code + $scope.kode_ea + data.no + data.id_art;
                break;

        }
        if(typeof Media !='undefined' && $state.current.name=="survey.master"){

            Record.createFile( $scope.kode_ea+'_'+$scope.kode_petugas+'_'+kode_buku);
            Record.record( $scope.kode_ea+'_'+$scope.kode_petugas+'_'+kode_buku);
        }
        query = DB.query("UPDATE ms_master SET no =?, id_art=?, kode_buku=?, nama=?, spv=?, dusun=?, rw=?, rt=?, address=?, location=?, phone=?, hp=?, owner=?, is_telp=?, is_hp=?, tt=? WHERE id = ?",
                            [data.no, data.id_art, kode_buku, $scope.nama_spv, $scope.kode_spv, data.dusun, data.rw, data.rt, data.address, data.location, data.phone, data.hp, data.owner,data.is_telp, data.is_hp,data.tt, $stateParams.id_survey])
        .then(function(result){
                var first_header = Object.keys($scope.q)[0];

                if($scope.q[first_header].is_crosstab){
                    $state.go("survey.roster", {
                        id_buku: $scope.id_buku,
                        id_survey: $stateParams.id_survey,
                        id_header: 1,
                        id_quest: Object.keys($scope.q[first_header].quest)[0]
                    });
                }else{
                    $state.go("survey.quests", {
                        id_buku: $scope.id_buku,
                        id_survey: $stateParams.id_survey,
                        id_header: 1,
                        id_quest: Object.keys($scope.q[first_header].quest)[0]
                    });
                }
        });

    }
})
.controller('RemarkCtrl', function($scope, $stateParams, DB, $state, Master, Book) {
    $scope.id_survey = $stateParams.id_survey;
    $scope.id_buku = $stateParams.id_buku;
    $scope.insertOther = true;
    Master.remark($scope.id_survey, $scope.id_buku).then(function(result){
        $scope.remark = result;
    });
    $scope.insert = function(){
        $scope.displayForm = true;
        $scope.insertOther = false;
        $scope.saveOther = true;
    }
    $scope.kode_petugas = $.jStorage.get('kode');
    $scope.save = function(data){
        Record.stop(true);
        query = DB.query("INSERT INTO ms_remark(id_survey, id_buku, variable,remark,entry_stamp,edit_stamp) VALUES(?,?,?,?,?,?)",
            [$scope.id_survey, $scope.id_buku,  data.variable.toString().toUpperCase(), data.remark,  moment().format('YYYY-MM-DD HH:mm:ss'),null]
        ).then(function(result){
            code = Object.keys($scope.remark).length;
            p_key = code;
            $scope.remark[p_key] = {"variable":data.variable.toString().toUpperCase(), "remark":data.remark};


        });



    }

})
.controller('BukuCtrl', function($scope, Book, $stateParams, DB, $state) {
    $scope.id_survey = $stateParams.id_survey;
    $scope.buku = {};
    Book.data.then(function(response){
        $scope.buku = response.data;
    })
}).controller('BackupCtrl', function($scope, Book, $stateParams, DB, $state, $q, $ionicLoading) {
    $scope.kode_petugas = $.jStorage.get('kode');

    $scope.kode_ea = $.jStorage.get('ea');
    $scope.export = function(){
        $ionicLoading.show();
        var data = {};

        var master = DB.query('SELECT * FROM ms_master WHERE id_ea  = ? AND id_enumerator = ?',[$scope.kode_ea, $scope.kode_petugas])
        .then(function(result){

            data.master = DB.fetchAll(result);
            angular.forEach(data.master, function(value, key){

                results = DB.query('SELECT * FROM ms_result WHERE id_survey = ?',[value.id]).then(function(result){

                    data.master[key].results = DB.fetchAll(result);
                    return
                });
                history = DB.query('SELECT * FROM ms_quest_history WHERE id_survey = ?',[value.id]).then(function(result){
                      data.master[key].history = DB.fetchAll(result);
                 });
                data_remark = DB.query('SELECT * FROM ms_remark WHERE id_survey = ?',[value.id]).then(function(result){
                    data.master[key].data_remark = DB.fetchAll(result);
                });
                crosstab = DB.query('SELECT * FROM ms_crosstab WHERE id_survey = ?',[value.id]).then(function(result){
                    data.master[key].crosstab = DB.fetchAll(result);
                });
                riwayat = DB.query('SELECT * FROM ms_riwayat WHERE id_survey = ?',[value.id]).then(function(result){
                    data.master[key].riwayat = DB.fetchAll(result);
                });
                 master_history = DB.query('SELECT * FROM ms_master_history WHERE id_survey = ?',[value.id]).then(function(result){
                     data.master[key].master_history = DB.fetchAll(result);
                 });
            })
            return data;
        });


//
//
         master.then(function(){
            var nama_file= "export_"+$scope.kode_petugas+"_"+$scope.kode_ea+"_"+moment().format('YYYY_MM_DD_HH_mm_ss')+".json";
            window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory , function (dirEntry) {
                console.log(nama_file)
                var isAppend = true;
                createFile(dirEntry, nama_file, true);
                function createFile(dirEntry, fileName, isAppend) {
                     // Creates a new file or returns the file if it already exists.
                     dirEntry.getFile(nama_file, {create: true, exclusive: false}, function(fileEntry) {

                         writeFile(fileEntry, JSON.stringify(data), isAppend);

                     }, function(error){
                         console.log(error);
                         $ionicLoading.hide();

                     });

                 }
                 function writeFile(fileEntry, dataObj) {
                    // Create a FileWriter object for our FileEntry (log.txt).
                    fileEntry.createWriter(function (fileWriter) {

                        fileWriter.onwriteend = function() {
                            console.log("Successful file write...");
                            alert('Sukses export Data');
                            $ionicLoading.hide();
                        };

                        fileWriter.onerror = function (e) {
                            console.log("Failed file write: " + e.toString());
                            $ionicLoading.hide();
                        };

                        // If data object is not passed in,
                        // create a new Blob instead.
                        if (!dataObj) {
                            dataObj = new Blob(['some file data'], { type: 'text/plain' });
                        }

                        fileWriter.write(dataObj);

                    });
                }
            }, function(error){
              console.log(error)
            });
         });



    }

});
