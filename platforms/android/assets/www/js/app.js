// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ui.router','ngMask', 'ui.mask'] )

    .run(function($ionicPlatform, DB, $templateCache, $rootScope) {
        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
             // DB.query('DROP TABLE ms_crosstab');
             DB.init();
            $rootScope.$on('$viewContentLoaded', function() {
		      $templateCache.removeAll();
		   });
        });
    })

    .config(function($stateProvider, $urlRouterProvider, $httpProvider ) {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider
            // setup an abstract state for the tabs directive
            .state('survey', {
                url: '/survey',
                abstract: true,
                cache: false,
                templateUrl: 'templates/survey.html',
                // controller: 'QuestCtrl'
            })
            .state('survey.login', {
            	cache: false,
                url: '/login',
                views: {
			    	'menuContent': {
			        	templateUrl: 'templates/login.html',
			        	controller: 'LoginCtrl'
			    	}
			    }
            })
             .state('survey.dash', {
             	cache: false,
                url: '/dash',
                views: {
			    	'menuContent': {
			        	templateUrl: 'templates/dash.html',
			        	controller: 'DashCtrl'
			    	}
			    }
            })
       //       .state('survey.start', {

       //          url: '/start',
       //          views: {
			    // 	'menuContent': {
			    //     	templateUrl: 'templates/start.html',
			    //     	controller: 'MasterCtrl'
			    // 	}
			    // }
       //      })
            .state('survey.start', {
            	cache: false,
                url: '/start/:id_buku',
                views: {
			    	'menuContent': {
			        	templateUrl: 'templates/start.html',
			        	controller: 'MasterCtrl'
			    	}
			    }
            })
             .state('survey.consent', {
             	cache: false,
                url: '/consent/:id_buku',
                views: {
			    	'menuContent': {
			        	templateUrl: 'templates/consent_form.html',
			        	controller: 'MasterCtrl'
			    	}
			    }
            }) .state('survey.master', {
             	cache: false,
                url: '/master/:id_buku/:id_survey',
                views: {
			    	'menuContent': {
			        	templateUrl: 'templates/master.html',
			        	controller: 'MasterCtrl'
			    	}
			    }
            }).state('survey.buku', {
            	cache: false,
                url: '/buku/',
                views: {
			    	'menuContent': {
			        	templateUrl: 'templates/buku.html',
			        	controller: 'BukuCtrl'
			    	}
			    }
            })
           	.state('survey.quests', {
           		cache: false,
                url: '/quests/:id_buku/:id_survey/:id_header/:id_quest/:id_roster',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/survey-quests.html',
                        controller: 'QuestCtrl'
                    }
                }
            }).state('survey.remark', {
            	cache: false,
                url: '/remark/:id_survey/:id_buku',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/remark.html',
                        controller: 'RemarkCtrl'
                    }
                }
            }).state('survey.roster', {
            	cache: false,
                url: '/roster/:id_survey/:id_buku/:id_header/:id_quest/:id_roster',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/roster.html',
                        controller: 'QuestCtrl'
                    }
                }
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/survey/login');

    }).constant('DB_CONFIG', {
	    name: 'DB',
	    tables: [{
	            name: 'ms_master',
	            columns: [
	                {name: 'id', type: 'INTEGER PRIMARY KEY'},
	                {name: 'id_buku', type: 'INTEGER'},
	                {name: 'id_enumerator', type: 'TEXT'},
	                {name: 'id_ea', type: 'TEXT'},
	                {name: 'id_rt', type: 'TEXT'},
	                {name: 'id_art', type: 'TEXT'},
	                {name: 'kode_buku', type: 'TEXT'},
	                {name: 'no', type: 'TEXT'},
	                {name: 'nama', type: 'TEXT'},
	                {name: 'spv', type: 'TEXT'},
	                {name: 'dusun', type: 'TEXT'},
	                {name: 'rw', type: 'TEXT'},
	                {name: 'rt', type: 'TEXT'},
	                {name: 'address', type: 'TEXT'},
	                {name: 'location', type: 'TEXT'},
	                {name: 'phone', type: 'TEXT'},
	                {name: 'hp', type: 'TEXT'},
	                {name: 'owner', type: 'TEXT'},
	                {name: 'is_hp', type: 'TEXT'},
	                {name: 'is_telp', type: 'TEXT'},
	                {name: 'tt', type: 'TEXT'},
	                {name: 'answer', type: 'TEXT'},
	                {name: 'remark', type: 'TEXT'},
	                {name: 'id_last_header','type':'INTEGER'},
	                {name: 'id_key_header','type':'INTEGER'},
	                {name: 'id_last_quest','type':'INTEGER'},
	                {name: 'id_last_roster','type':'INTEGER'},
	                {name: 'entry_stamp', type: 'TEXT'},
	                {name: 'edit_stamp', type: 'TEXT'}
	            ]
	        },{
	            name: 'ms_remark',
	            columns: [
	                {name: 'id', type: 'INTEGER PRIMARY KEY'},
	                {name: 'id_survey', type: 'INTEGER'},
	                {name: 'id_buku', type: 'INTEGER'},
	                {name: 'id_quest', type: 'TEXT'},
	                {name: 'variable', type: 'TEXT'},
	                {name: 'remark', type: 'TEXT'},
	                {name: 'entry_stamp', type: 'TEXT'},
	                {name: 'edit_stamp', type: 'TEXT'}
	            ]
	        },
	    	{
	            name: 'ms_result',
	            columns: [
	                {name: 'id', type: 'INTEGER PRIMARY KEY'},
	                {name: 'id_survey', type: 'INTEGER'},
	                {name: 'kode_petugas', type: 'INTEGER'},
	                {name: 'id_buku', type: 'INTEGER'},
	                {name: 'id_header', type: 'INTEGER'},
	                {name: 'key_header', type: 'INTEGER'},
	                {name: 'id_quest', type: 'INTEGER'},
	                {name: 'key', type: 'INTEGER'},
	                {name: 'key_multiple', type: 'INTEGER'},
	                {name: 'id_crosstab', type: 'TEXT'},
	                {name: 'is_tt', type: 'TEXT'},
	                {name: 'is_tb', type: 'TEXT'},
	                {name: 'is_mm', type: 'TEXT'},
	                {name: 'value', type: 'TEXT'},
					{name: 'value_2', type: 'TEXT'},
					{name: 'value_3', type: 'TEXT'},
					{name: 'prev_quest', type: 'TEXT'},
					{name: 'prev_header', type: 'TEXT'},
	                {name: 'entry_stamp', type: 'TEXT'},
	                {name: 'edit_stamp', type: 'TEXT'}
	            ]
	        },
	        {
	            name: 'ms_crosstab',
	            columns: [
	                {name: 'id', type: 'INTEGER PRIMARY KEY'},
	                {name: 'id_survey', type: 'INTEGER'},
	                {name: 'kode_petugas', type: 'INTEGER'},
	                {name: 'id_buku', type: 'INTEGER'},
	                {name: 'id_header', type: 'INTEGER'},
	                {name: 'key_header', type: 'INTEGER'},
	                {name: 'id_roster', type: 'INTEGER'},
	                {name: 'value', type: 'TEXT'},
	                {name: 'code', type: 'TEXT'},
	                {name: 'status', type: 'TEXT'},
	                {name: 'prev_quest', type: 'TEXT'},
					{name: 'prev_header', type: 'TEXT'},
	                {name: 'entry_stamp', type: 'TEXT'},
	                {name: 'edit_stamp', type: 'TEXT'}
	            ]
	        },
	        {
	        	name: 'ms_riwayat',
	            columns: [
	                {name: 'id', type: 'INTEGER PRIMARY KEY'},
	                {name: 'id_survey', type: 'INTEGER'},
	                {name: 'order', type: 'INTEGER'},
	                {name: 'status', type: 'INTEGER'},
	                {name: 'date', type: 'TEXT'},
	                {name: 'time_start', type: 'TEXT'},
	                {name: 'time_end', type: 'TEXT'},
	                {name: 'remark', type: 'TEXT'},
	                {name: 'entry_stamp', type: 'TEXT'},
	                {name: 'edit_stamp', type: 'TEXT'}
	            ]
	        },
	         {
	        	name: 'ms_quest_history',
	            columns: [
	                {name: 'id', type: 'INTEGER PRIMARY KEY'},
	                {name: 'id_survey', type: 'INTEGER'},
	                {name: 'id_header', type: 'INTEGER'},
	                {name: 'id_quest', type: 'INTEGER'},
	                {name: 'next_header', type: 'INTEGER'},
	                {name: 'next_quest', type: 'INTEGER'},
	                {name: 'entry_stamp', type: 'TEXT'},
	                {name: 'edit_stamp', type: 'TEXT'}
	            ]
	        }
	    ]
	}).directive('generateQuest', function($compile, Answer, $state) {

		var linker = function(scope, element, attrs, ctrl){
			scope.warningMsg = {};
			scope.totalMsg = Object.keys(scope.warningMsg).length;
			value = scope.value;
			quest = scope.quest;
			scope.fill = {};

			
			Answer.getByIdQuestion(scope.idsurvey, scope.idheader, scope.idquest, scope.key, scope.idroster, scope.quest).then(function(result){
				
				scope.value.value = '';
				scope.fill = result;
				// console.log(result);
				if(typeof result != 'undefined'){

					if(scope.value.type=="checkbox"){
						scope.quest.content[result.key].value = (result.value === 'true');

						if(typeof scope.value.next_to !='undefined' && scope.value.value == true){

							scope.next_to(scope.value)
						}
					}
					else if(scope.value.type=="other"){
						
						scope.quest.content[result.key].value = (result.value === 'true');					
						scope.quest.content[result.key].value_2 = result.value_2;									
						scope.quest.content[result.key].value_3 = result.value_3;									
					}else{
						if(scope.value.replicate > 0){

							scope.value.value = [];
							scope.value.value_2 = [];
							angular.forEach(result, function(value, key){
								scope.quest.content[value.key].value[value.key_multiple] = value.value;								
								scope.quest.content[value.key].value_2[value.key_multiple] = value.value_2;								
							})

							
						}else{
							if(typeof scope.quest.content[result.key]!='undefined'){
								if( result.value_2!=""){
									scope.quest.content[result.key].value_2 = result.value_2;	
								}
								scope.quest.content[result.key].value = result.value;
							}
									
						}
						
					}
					if(typeof result.value !='undefined'){
						if(result.value!=''&&scope.$parent.$parent.$parent.is_valid == true){

							scope.$parent.$parent.$parent.is_valid = false;
						}
					}
				}
			});

			function create(){
				
				
				scope.checkOther = function(value){
					
					if(value.value!=''&&scope.$parent.$parent.$parent.is_valid == true){
						scope.$parent.$parent.$parent.is_valid = false;
					}
						

					angular.forEach(quest.content, function(value, key){

						if(value.is_absolute){
							if(key!=scope.key && (value.type=='checkbox' || value.type=='other' || value.type=='text')){

								if(quest.content[key].type=='checkbox'){
									scope.quest.content[key].value = false;
								}else if(quest.content[key].type=='other'){
									scope.quest.content[key].value = false;
									scope.quest.content[key].value_2 = '';
									scope.quest.content[key].value_3 = '';
								}else{
									scope.quest.content[key].value = '';
								}
								
							}
							
						}
					})

					
					if(value.next_to=='finish'){

						if(scope.idroster!=''){
							scope.$parent.$parent.$parent.roster = true;
					    	scope.$parent.$parent.$parent.nextQuest = Object.keys(scope.$parent.$parent.$parent.data[scope.$parent.$parent.$parent.key_header].quest)[0];
					    	scope.$parent.$parent.$parent.nextHeader = scope.$parent.$parent.$parent.key_header;
						}else{


							if(typeof scope.$parent.$parent.$parent.data[scope.$parent.$parent.$parent.header.next] != 'undefined'){
								if(scope.$parent.$parent.$parent.data[scope.$parent.$parent.$parent.header.next].is_crosstab==true){
									scope.$parent.$parent.$parent.roster = true;
								}
							}
							
							scope.$parent.$parent.$parent.nextHeader = scope.$parent.$parent.$parent.header.next;

							if(typeof scope.$parent.$parent.$parent.nextHeader !='undefined'){
								
								scope.$parent.$parent.$parent.nextQuest = Object.keys(scope.$parent.$parent.$parent.data[scope.$parent.$parent.$parent.nextHeader].quest)[0];
							}
							
						}

						if(scope.$parent.$parent.$parent.header.last==true){
							scope.$parent.$parent.$parent.end = true;
						}
					}else{
						
						if(typeof value.next_to !='undefined'){
							scope.$parent.$parent.$parent.nextQuest = value.next_to;	
						}else{
							scope.$parent.$parent.$parent.nextQuest = scope.$parent.$parent.$parent.quest.next;
						}
						
						scope.$parent.$parent.$parent.nextHeader = scope.$parent.$parent.$parent.key_header;	
						if(scope.$parent.$parent.$parent.quest.last==true){
	                        scope.$parent.$parent.$parent.nextHeader = scope.$parent.$parent.$parent.header.next;
	                       
	                        scope.$parent.$parent.$parent.nextQuest = Object.keys(scope.$parent.$parent.$parent.data[scope.$parent.$parent.$parent.header.next].quest)[0];
	                       
	                    }
	                    if(scope.$parent.$parent.$parent.id_roster!=''&&scope.$parent.$parent.$parent.quest.last==true){

							// scope.$parent.$parent.$parent.roster = true;
					    	scope.$parent.$parent.$parent.nextQuest = Object.keys(scope.$parent.$parent.$parent.data[scope.$parent.$parent.$parent.key_header].quest)[0];
					    	scope.$parent.$parent.$parent.nextHeader = scope.$parent.$parent.$parent.key_header;
						}
						
					}


				}
				scope.setVal = function(value, text){
					// scope.value.value = text;
				}
				scope.next_to = function(value, is_proses){
					
					if(value.value==false){
						return false;
					}
					
					if(typeof value.next_to !='undefined'){

						if(value.next_to=='finish'){

							if(scope.idroster!=''){
								scope.$parent.$parent.$parent.roster = true;
						    	scope.$parent.$parent.$parent.nextQuest = Object.keys(scope.$parent.$parent.$parent.data[scope.$parent.$parent.$parent.key_header].quest)[0];
						    	scope.$parent.$parent.$parent.nextHeader = scope.$parent.$parent.key_header;
							}else{

								if(typeof scope.$parent.$parent.$parent.data[scope.$parent.$parent.$parent.header.next] != 'undefined'){
									if(scope.$parent.$parent.$parent.data[scope.$parent.$parent.$parent.header.next].is_crosstab==true){
										scope.$parent.$parent.$parent.roster = true;
									}
								}
								
								if(typeof scope.$parent.$parent.$parent.header.next !='undefined'){
									scope.$parent.$parent.$parent.nextHeader = scope.$parent.$parent.$parent.header.next;
									scope.$parent.$parent.$parent.nextQuest = Object.keys(scope.$parent.$parent.$parent.data[scope.$parent.$parent.$parent.nextHeader].quest)[0];
								}
								
							}
							
							if(scope.$parent.$parent.$parent.header.last==true){

								scope.$parent.$parent.$parent.end = true;
								// scope.$parent.$parent.$parent.finish();
							}
						}else{

							scope.$parent.$parent.$parent.nextQuest = value.next_to;
							scope.$parent.$parent.$parent.nextHeader = scope.$parent.$parent.$parent.key_header;	
						}

					}else{

						if(scope.$parent.$parent.header.next>0&&scope.$parent.$parent.quest.last==true){
		                    scope.$parent.$parent.$parent.nextHeader = scope.$parent.$parent.$parent.header.next;
		                    scope.$parent.$parent.$parent.nextQuest = Object.keys(scope.$parent.$parent.data[scope.$parent.$parent.header.next].quest)[0];
		                
		                }

		                if(scope.$parent.$parent.$parent.quest.next>0){
		                	scope.$parent.$parent.$parent.roster = false;
		                    scope.$parent.$parent.$parent.$parent.nextQuest = scope.$parent.$parent.$parent.quest.next;
		                    
		                }
		                if(scope.$parent.$parent.$parent.id_roster!=''&&scope.$parent.$parent.$parent.quest.last==true){

							scope.$parent.$parent.$parent.roster = true;
					    	scope.$parent.$parent.$parent.nextQuest = Object.keys(scope.$parent.$parent.$parent.data[scope.$parent.$parent.$parent.key_header].quest)[0];
					    	scope.$parent.$parent.$parent.nextHeader = scope.$parent.$parent.$parent.key_header;
						}

					}

					
	                if(scope.$parent.$parent.$parent.header.last && scope.$parent.$parent.$parent.quest.next_to =='finish'){
	                    scope.$parent.$parent.$parent.end = true;
	                }else{
	                     scope.$parent.$parent.$parent.end = false;
	                }
	                if(value.is_absolute && value.type=="checkbox" && is_proses){
	                	scope.$parent.$parent.save();
	                	
	                	if(scope.$parent.$parent.$parent.header.last && value.next_to =='finish'){

	                	
	                		$state.go("survey.remark", {
								id_survey: scope.idsurvey, 
								id_buku: scope.$parent.$parent.$parent.id_buku,
			                });
			                
	                	}else if(scope.$parent.$parent.roster ||scope.$parent.$parent.end){
	                		$state.go("survey.roster", {
								id_survey: scope.idsurvey, 
								id_buku: scope.$parent.$parent.$parent.id_buku,
								id_header: scope.$parent.$parent.$parent.nextHeader,
								id_quest: scope.$parent.$parent.$parent.nextQuest
			                });
						}else{
							$state.go("survey.quests", {
								id_survey: scope.idsurvey, 
								id_buku: scope.$parent.$parent.$parent.id_buku,
								id_header: scope.$parent.$parent.$parent.nextHeader,
								id_quest: scope.$parent.$parent.$parent.nextQuest
			                });
						}
						
					}

				}
				
				switch(value.type){

					case 'text': 


						additional = '';
						var _format = '';
						var _type = 'text';
						var maxlength = '';
						if(typeof value.is_numeric !='undefined'&&typeof value.is_mask !='undefined'){
							if(value.is_numeric==true && value.is_mask==true){
								// _format = ' format="number"'
							}
							
							if(value.is_numeric==true && (value.is_mask!=true && value.is_mask)){
								_type = 'number';
							}else{
								_type = 'text';
							}
						}
						
						if(value.maxlength>0){
							maxlength = "maxlength = '"+maxlength+"'";
						}

						
						if(value.replicate==null || value.replicate=='0'){
							var mask = '';
							if(value.mask!='' &&typeof value.mask !='undefined'){
								mask +="ui-mask='"+value.mask+"'" ;
								
							}
							var max = '';
							if(value.max){
								max +=' max="'+value.max+'" ';
							}
							var min = '';
							if(value.min){
								min +=' min="'+value.min+'" ';
							}
							var fn = '';
							if(value.fn){
								fn = value.fn;
							}
							var ngchange = '';
							if(value.ngchange){
								ngchange = value.ngchange;
							}
							var minval = '';
								if(value.minval){
									minval +=' minval="'+value.minval+'" ';
								}
							var maxval = '';
								if(value.maxval){
									maxval +=' maxval="'+value.maxval+'" ';
								}
							additional='';
							console.log(value);
							if((value.label_2!='' ||value.suffix!='')&& value.is_label_2 ){
								var mask_2 = 'mask=""';
								if(value.mask_2!='' && typeof value.mask_2 !='undefined'){
									var mask_2="mask='"+value.mask_2+"'" ;
								}
								var max = '';
								if(value.max){
									max +=' max="'+value.max+'" ';
								}
								var min = '';
								if(value.min){
									min +=' min="'+value.min+'" ';
								}

								var fn = '';
								if(value.fn){
									fn = value.fn;
								}

								additional +='<div class="input-group"><span class="input-label">{{value.label_2}}</span><input type="'+_type+'" ng-model="value.value_2" '+mask_2+' '+fn+' '+max+'> <span class="input-group-addon ">{{value.suffix_2}}</span></div>';
								
							}
							if(value.label==''||value.label==null){
								
								element.append('<div class="item item-input item-stacked-label"><div class="input-group"><span class="input-label leftLabel">{{value.code}} </span><input type="'+_type+'" ng-model="value.value" '+mask+' '+fn+ ' '+minval+' '+maxval+' class="input-addon isChange " '+maxlength+' '+_format+' '+max+' '+min+' autocapitalize="on" ng-change="checkOther(value); '+ngchange+'">'+additional+'<span class="input-group-addon ">{{value.suffix}}</span></div>');
								
							}else{
								element.append('<div class="item item-input item-stacked-label"><span class="input-label">{{value.code + (value.code!="" ? "." : "")}} {{value.label}}</span><input type="'+_type+'" ng-model="value.value" '+mask+' '+fn+ ' '+minval+' '+maxval+' '+maxlength+' '+max+' '+min+'  ng-change="checkOther(value)" '+_format+' required>'+additional);
							}

						}else{
							var mask = '';
							if(value.mask!='' && value.mask!=null && typeof value.mask !='undefined'){
								switch(value.mask){
									case 'date':
										mask += 'my-date';
										break;
									default:
										mask +="mask='"+value.mask+"'" ;
								}
								
							}
							var max = '';
							if(value.max){
								max +=' max="'+value.max+'" ';
							}

							var min = '';
							if(value.min){
								min +=' min="'+value.min+'" ';
							}
							var fn = '';
							if(value.fn){
								fn = value.fn;
							}
							var ngchange = '';
							if(value.ngchange){
								ngchange = value.ngchange;
							}

							for(i=1; i<=value.replicate; i++){

								additional='';
								if(value.label_2!='' ||value.suffix!=''|| value.is_label_2){
									var mask_2 = 'mask=""';
									if(value.mask_2!='' && typeof value.mask_2 !='undefined'){
										var mask_2="mask='"+value.mask_2+"'" ;
									}
									var max = '';
									if(value.max){
										max +=' max="'+value.max+'" ';
									}
									var min = '';
									if(value.min){
										min +=' min="'+value.min+'" ';
									}
									var fn = '';
									if(value.fn){
										fn = value.fn;
									}

									additional +='<div class="input-group"><span class="input-label">{{value.label_2}}</span><input type="'+_type+'" ng-model="value.value_2['+i+']" '+mask_2+' '+fn+' '+max+'> <span class="input-group-addon ">{{value.suffix_2}}</span></div>';
								}
								if(value.label==''||value.label==null){
									
									element.append('<div class="item item-input item-stacked-label"><div class="input-group"><span class="input-label leftLabel">{{value.code}} </span><input type="'+_type+'" ng-model="value.value['+i+']" '+mask+' '+fn+ ' class="input-addon isChange " '+maxlength+' '+_format+' '+max+' '+min+' autocapitalize="on" ng-change=" '+ngchange+'">'+additional+'<span class="input-group-addon ">{{value.suffix}}</span></div>');
									
								}else{
									element.append('<div class="item item-input item-stacked-label"><span class="input-label">{{value.code}}. {{value.label}}</span><input type="'+_type+'" ng-model="value.value['+i+']" '+maxlength+' '+_format+' required>'+additional);
								}
							}
						}
						
						
						break
					case 'checkbox':
						is_next = '';
						if(value.is_absolute){
							var is_next = 'proses_next(value)';
						}
						element.append('<ion-checkbox ng-model="value.value" ng-click="checkOther(value);next_to(value,true); '+is_next+'" >{{value.code}}. {{value.label}}</ion-checkbox>');
						break;
					case 'other':
						var mask = '';
						if(value.mask!='' &&typeof value.mask !='undefined'){
							switch(value.mask){
								case 'date':
									mask += 'my-date';
									break;
								default:
									mask +="mask='"+value.mask+"'" ;
							}
							
						}
						var max = '';
						if(value.max){
							max +=' max="'+value.max+'" ';
						}
						var min = '';
						if(value.min){
							min +=' min="'+value.min+'" ';
						}
						var fn = '';
						if(value.fn){
							fn = value.fn;
						}
						var ngchange = '';
						if(value.ngchange){
							ngchange = value.ngchange;
						}
						var additional = '';
						if(value.label_2!=''||value.suffix_2!=''){
							var mask_2 = 'mask=""';
							if(value.mask_2!='' && typeof value.mask_2 !='undefined'){
								var mask_2="mask='"+value.mask_2+"'" ;
							}
							var max_2 = '';
							if(value.max_2){
								max_2 +=' max="'+value.max_2+'" ';
							}
							var min_2 = '';
							if(value.min_2){
								min_2 +=' min="'+value.min_2+'" ';
							}
							
							additional +='<div class="wrapper"><span class="input-label">{{value.label_2}}</span><input type="text" class="input-addon" ng-model="value.value_3" '+mask_2+' '+fn+ ' '+max_2+' '+min_2+'> <span class="input-group-addon ">{{value.suffix_2}}</span></div>';
						}
						element.append('<ion-checkbox ng-model="value.value" ng-click="checkOther(value);next_to(value,true)" >{{value.code}}. {{value.label}}</ion-checkbox><div class="input-group item item-input" ng-show="answer"><div class="wrapper"><input  type="text" ng-model="value.value_2" class="others input-addon" '+mask+' '+fn+ ' '+max+' '+min+'><span class="input-group-addon ">{{value.suffix}}</span></div>'+additional+'</div>');
						break;
				}

				element.append('<div class="alert alert-warning" ><ul><li ng-repeat="(key, value) in warningMsg">{{value}}</li></ul></div>');
				if(value.value!=''&&scope.$parent.$parent.$parent.is_valid == true){

					scope.$parent.$parent.$parent.is_valid = false;
				}
			}

			create(value);

			
			$compile(element.contents())(scope);
			
          
        };
        return {
        	require: '?ngModel',
            scope: {
                value: '=',
                idquest:'=',
                idheader:'=',
                idsurvey:'=',
                idroster:'=',
                key:'=',
                answer:'=',
                quest:'=',
                wrap: '='
            },
            replace: true,
           	link: linker

        };
    }).directive('myDate', ["$filter", "$parse", function dateInput($filter, $parse) {
    return {
		restrict: 'A',
		require: '?ngModel',
		replace: true,
		transclude: true,
		template: '<input ng-transclude ui-mask="**/**/****" ui-mask-raw="false" placeholder="DD/MM/YYYY"/>',
		link: function(scope, element, attrs, controller) {
      		var oldVal = '';
			// scope.limitToValidDate = limitToValidDate;
			var dateFilter = $filter("date");
			var today = new Date();
			var date = {};


			var pattern = "^(0[1-9]|[12][0-9]|3[01]|[Tt][BbTt]|[Mm]{2})(0[1-9]|1[012]|[Tt]{2}|[Mm]{2})(18|19|20)\\d\\d$" +
			  "|^(0[1-9]|[12][0-9]|3[01]|[Tt]{2}|[Mm]{2})(0[1-9]|1[012]|[Tt]{2}|[Mm]{2})(18|19|20)\\d$" +
			  "|^(0[1-9]|[12][0-9]|3[01]|[Tt]{2}|[Mm]{2})(0[1-9]|1[012]|[Tt]{2}|[Mm]{2})(18|19|20|[TtMm]{2})$" +
			  "|^(0[1-9]|[12][0-9]|3[01]|[Tt]{2}|[Mm]{2})(0[1-9]|1[012]|[Tt]{2}|[Mm]{2})([12TtMm])$" +
			  "|^(0[1-9]|[12][0-9]|3[01]|[Tt]{2}|[Mm]{2})(0[1-9]|1[012]|[Tt]{2}|[Mm]{2})$" + 
			  "|^(0[1-9]|[12][0-9]|3[01]|[Tt]{2}|[Mm]{2})([0-1TtMm])$" + //ddM
			  "|^(0[1-9]|[12][0-9]|3[01]|[Tt]{2}|[Mm]{2})$" + //dd
			  "|^[0-3TtMm]$"; //d
			var regexp = new RegExp(pattern);

			element.on("keyup", function(e) {

	      		val = element.val().trim().replace(/\s|\//g, "");

		    	if (!(regexp.test(val)) && e.keyCode != 46 && e.keyCode != 8 ){
		          	element.val(oldVal);
		        } else {
		          
		          	oldVal = element.val();
		        }
	      	});
		}
    }
  }]).directive('monthYear', ["$filter", "$parse", function dateInput($filter, $parse) {
    return {
		restrict: 'A',
		require: '?ngModel',
		replace: true,
		transclude: true,
		template: '<input ng-transclude ui-mask="**/****" ui-mask-raw="false" placeholder="MM/YYYY"/>',
		link: function(scope, element, attrs, controller) {
      		var dateFilter = $filter("date");
			var today = new Date();
			var date = {};
			var oldVal = '';
			var pattern = "^(0[1-9]|1[012]|[Tt]{2}|[Mm]{2})(18|19|20)\\d\\d$" +
			  "|^(0[1-9]|1[012]|[Tt]{2}|[Mm]{2})(18|19|20)\\d$" +
			  "|^(0[1-9]|1[012]|[Tt]{2}|[Mm]{2})(18|19|20)$" +
			  "|^(0[1-9]|1[012]|[Tt]{2}|[Mm]{2})[12]$" +
			  "|^(0[1-9]|1[012]|[Tt]{2}|[Mm]{2})$" + 
			  "|^([0-1TtMm])$";
			var regexp = new RegExp(pattern);
			element.on("keyup", function(e) {

	      		val = element.val().trim().replace(/\s|\//g, "");
	      		// console.log
		    	if (!(regexp.test(val)) && e.keyCode != 46 && e.keyCode != 8 ){
		          	element.val(oldVal);
		        } else {
		          
		          	oldVal = element.val();
		        }
	      	});


		}
    }
  }]).directive('duaDigit',  ["$filter", "$parse",  function($filter, $parse){
	return{
			restrict: 'A',
		require: '?ngModel',
		link: function(scope, element, attrs, Controller) {
			var oldVal = '';
	      	var pattern =  	"^([0-9]{1,2}|[Tt]{2}|[Mm]{2}|[TtBb]{2})$" + 
	      					"|^([0-9TtMm]{1,2})$"; 
			var regexp = new RegExp(pattern);

	      	element.on("keyup", function(e) {
	      		val = element.val().trim();
		    	if (!(regexp.test(val)) && e.keyCode != 46 && e.keyCode != 8 ){
		          	element.val(oldVal);
		        } else {
		          
		          	oldVal = element.val();
		        }
	      	});
			
		  	
        }
	}
}]).directive('yearMask', ["$filter", "$parse", function dateInput($filter, $parse) {
    return {
		restrict: 'A',
		require: '?ngModel',
		replace: true,
		transclude: true,
		template: '<input ng-transclude ui-mask="****" ui-mask-raw="false" ng-keypress="limitToValidDate($event)" placeholder="YYYY"/>',
		link: function(scope, element, attrs, controller) {
      	
			scope.limitToValidDate = limitToValidDate;
			var dateFilter = $filter("date");
			var today = new Date();
			var date = {};

			var pattern = "^(18|19|20)\\d\\d$" +
			  "|^(18|19|20)\\d$" +
			  "|^(18|19|20|[Tt]{2})$" +
			  "|^([12]|[Tt]{1,2})$"; //d
			var regexp = new RegExp(pattern);

			function limitToValidDate(event) {
				
				var key = event.charCode ? event.charCode : event.keyCode;
					if ((key >= 48 && key <= 57) || key === 9 || key === 46 || key===84||key===116||key===98||key===66||key===109||key===77) {

					var character = String.fromCharCode(event.which);
					character = character.toUpperCase();

					var start = element[0].selectionStart;
					var end = element[0].selectionEnd;
					var testValue = (element.val().slice(0, start) + character + element.val().slice(end)).replace(/\s|\//g, "");

					if (!(regexp.test(testValue))) {
					  	event.preventDefault();
					}

				}else{
					event.preventDefault();
				}
			}


		}
    }
  }]).directive('isAgeBelow',function (Master) {
    return {
    	require: '?ngModel',
      	link: function(scope, element, attrs, controller) {
      		console.log(attrs);
      		scope.checkagebelow = checkagebelow;
      		var _w = scope.$watch(value.value, checkagebelow)
      		
      		function checkagebelow(newVal, oldVal){
      			
		      	Master.getById(scope.idsurvey).then(function(res){
		      		
		      		var a = moment(newVal.value,'DD/MM/YYYY');
		      		var b = moment(res.entry_stamp);
		      		console.log(scope);
		      		diff = b.diff(a, 'years')
		      		if(diff<5){
		      			scope.value.next_to = 'finish';
		      			scope.next_to(scope.$parent.value);
		      		}
		      	})	
      		}	
      	}
    }
  })
  .directive('isDayInWeek', ["$filter", "$parse", function dateInput($filter, $parse) {
    return {
		restrict: 'A',
		require: '?ngModel',
		replace: true,
		transclude: true,
		template: '<input ng-transclude ui-mask="**" ui-mask-raw="false" ng-keypress="isDayInWeek($event)"/>',
		link: function(scope, element, attrs, controller) {
      		element.on('keyup', function(e){
				isDayInWeek(e);
			})
			scope.isDayInWeek = isDayInWeek;

			var pattern = "^([1-7]|[Tt]{2}|[Mm]{2})$" + //dd
			  "|^[1-7TtMm]$"; //d

			var regexp = new RegExp(pattern);

			function isDayInWeek(event) {

				var key = event.charCode ? event.charCode : event.keyCode;
					if ((key >= 48 && key <= 57) || key === 9 || key === 46 || key===84||key===116||key===98||key===66||key===109||key===77) {

					var character = String.fromCharCode(event.which);
					character = character.toUpperCase();

					var start = element[0].selectionStart;
					var end = element[0].selectionEnd;
					var testValue = (element.val().slice(0, start) + character + element.val().slice(end)).replace(/\s|\//g, "");

					if (!(regexp.test(testValue))) {
					  	event.preventDefault();
					}

				}
			}


		}
    }
  }]).directive('isHourInDay', ["$filter", "$parse", function dateInput($filter, $parse) {
    return {
		restrict: 'A',
		require: '?ngModel',
		replace: true,
		transclude: true,
		template: '<input ng-transclude ui-mask="**" ui-mask-raw="true" ng-keypress="isHourInDay($event)"/>',
		link: function(scope, element, attrs, controller) {
      		element.on('keyup', function(e){
				isHourInDay(e);
			})
			scope.isHourInDay = isHourInDay;

			var pattern = "^(1[1-9]|2[1-4]|[Tt]{2}|[Mm]{2})$" + //dd
			  "|^[1-9TtMm]$"; //d

			var regexp = new RegExp(pattern);

			function isHourInDay(event) {

				var key = event.charCode ? event.charCode : event.keyCode;
				if ((key >= 48 && key <= 57) || key === 9 || key === 46 || key===84||key===116||key===98||key===66||key===109||key===77) {

					var character = String.fromCharCode(event.which);
					character = character.toUpperCase();

					var start = element[0].selectionStart;
					var end = element[0].selectionEnd;
					var testValue = (element.val().slice(0, start) + character + element.val().slice(end)).replace(/\s|\//g, "");
					
					if (!(regexp.test(testValue))) {
					  	event.preventDefault();
					}

				}else{
					if (!(regexp.test(testValue))) {
					  	event.preventDefault();
					}
				}
			}


		}
    }
}]).directive('numericDigit', ["$filter","$parse", function cekInput($filter, $parse) {
    return {
		restrict: 'A',
		require: '?ngModel',
		replace: true,
		transclude: true,
		scope: true,
		template: '<input ng-transclude ng-keypress="cekInput($event)"/>',
		link: function(scope, element, attrs, ngModel) {
			element.on('keyup', function(e){
				cekInput(e);
			})
			scope.cekInput = cekInput;
			function cekInput(event) {
				
				if(attrs.max > 0){
					str = '{1,'+attrs.max+'}';
				}else{
					str = '+';
				}

				var pattern = "^([0-9]"+str+"|[Tt]{1,2}|[Mm]{1,2})$"; 
				
				var regexp = new RegExp(pattern);
				var key = event.charCode ? event.charCode : event.keyCode;
				if ((key >= 48 && key <= 57) || key === 9 || key === 46 || key===84||key===116||key===98||key===66||key===109||key===77) {

					var character = String.fromCharCode(event.which);
					character = character.toUpperCase();

					var start = element[0].selectionStart;
					var end = element[0].selectionEnd;
					var testValue = (element.val().slice(0, start) + character + element.val().slice(end)).replace(/\s|\./g, "");
					
					if (!(regexp.test(testValue))) {
						
					  	event.preventDefault();
					}

				}else{
					event.preventDefault();
				}
			}
            
            var formatter = function(str) {	
                res =  str.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.');
                return res;
            }
            var updateView = function(val) {
                scope.$applyAsync(function () {
                    ngModel.$setViewValue(val);
                    ngModel.$render();
                });
            }
            var parseNumber = function(val) {

            	// var modelString = formatter(ngModel.$modelValue);            	
            	var newVal = val.replace(/[^0-9TtMm]/g,'');
            	var newVal = formatter(newVal);
            	
            	// if(val!=modelString){
            		updateView(newVal);
            	// }
            	// console.log(newVal);
            	return newVal;

            }
         	var formatNumber = function(val) {
         		return parseNumber(val)
            }
          
            ngModel.$parsers.push(parseNumber);
            ngModel.$formatters.push(formatNumber);
        }
    }
}]).directive('decimal', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
        	
            if (!ctrl) return;

            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue)
            });
            
            ctrl.$parsers.unshift(function (viewValue) {
            	
            	if(scope.is_decimal){
            		$(elem).priceFormat({
			            prefix: '',
			            centsSeparator: ',',
			            thousandsSeparator: '.'
			        });    
            	}else{
            		$(elem).priceFormat({
			            prefix: '',
			            centsLimit:0,
			            centsSeparator: ',',
			            thousandsSeparator: '.'
			        }); 
            	}
                 _val =  elem[0].value.replace(/\./g,"");

                 return _val.replace(/\,/g,".");

            });
        }
    };
}]).directive('underCurrTime', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
        	ctrl.$validators.dateInput
        }
    };
}]).directive('max', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
        	
        }
    };
}]).directive('isBelow', ['$filter','$compile', function ($filter, $compile) {
    return {
        require: '?ngModel',
        replace: true,
        link: function (scope, elem, attrs, ctrl) {
        	
        	function myValidation(value) {
        		var a = value.replace(/\./g,"");
        		var b = attrs.minval;
        		
                if (parseInt(a) > parseInt(b)) {

                	delete scope.$parent.warningMsg.isBelowMessage;
                    
                } else {
                	
                	scope.$parent.warningMsg.isBelowMessage = attrs.isBelowMessage
                    
                }
                return value;
            }

            ctrl.$parsers.push(myValidation);
        }
    };
}]).directive('isOver', ['$filter','$compile', function ($filter, $compile) {
    return {
        require: '?ngModel',
        replace: true,
        link: function (scope, elem, attrs, ctrl) {
        	
        	function myValidation(value) {
        		
        		var a = value.replace(/\./g,"");
        		var b = attrs.maxval;
                if (parseInt(a) > parseInt(b)) {
                	console.log(scope.$parent.totalMsg);
                	console.log(scope.$parent.warningMsg);
                	scope.$parent.warningMsg.isOverMessage = attrs.isOverMessage;

                } else {
                	delete scope.$parent.warningMsg.isOverMessage
                }

                return value;
            }

            ctrl.$parsers.push(myValidation);
        }
    };
}]).directive('isCondition', ['$filter','$compile','Answer', function ($filter, $compile, Answer) {
    return {
        require: '?ngModel',
        replace: true,
        link: function (scope, elem, attrs, ctrl) {
        	scope.x = null;
        	scope.y = null;
        	scope.z = null;
        	scope.aa = attrs.aa;
        	var _parent = scope.$parent.$parent.$parent.$parent;

        	Answer.getByAnswer(_parent.id_survey, attrs.x, _parent.id_roster, scope.$parent.quest).then(function(result){
        		if((attrs.x!='self')){
					scope.x = result.value;
				}
			});
			Answer.getByAnswer(_parent.id_survey, attrs.y, _parent.id_roster, scope.$parent.quest).then(function(result){
				if((attrs.y!='self')){
					scope.y = result.value;
				}		
        	});
        	Answer.getByAnswer(_parent.id_survey, attrs.z, _parent.id_roster, scope.$parent.quest).then(function(result){
				if((attrs.z!='self' && typeof result!='undefined')){
					scope.z = result.value;	
					console.log(result);

				}		
        	});

        	function myValidation(value) {

        		if((attrs.x=='self')){
        			scope.x = value.replace(/\./g,"")
        		}

        		if((attrs.y=='self')){
        			scope.y = value.replace(/\./g,"")
        		}

        		if((attrs.z=='self')){
        			scope.z = value.replace(/\./g,"")
        		}

        		var x = parseInt(scope.x);
        		var y = parseInt(scope.y);
        		var z = parseInt(scope.z);
        		
        		var parts = attrs.condition.split(/[[\]]{1,2}/);
        		var err = attrs.conditionMessage.split(/[[\]]{1,2}/);
        		var _err;

        		delete scope.$parent.warningMsg.conditionMessage;
        		var _errs = '';
        		delete parts[Object.keys(parts).length - 1]
        		delete parts[0];
        		var keepGoing = true;
        		angular.forEach(parts, function(value, key){
        			var cd = eval(value);
        			scope.aa = eval(attrs.aa);

	                if (typeof cd!='undefined' && cd && keepGoing) {
	                	var errs = err[key];

	                	errs = errs.toString().replace("{x}", scope.x);
	                	errs = errs.toString().replace("{y}", scope.y);
	                	errs = errs.toString().replace("{z}", scope.z);
	                	errs = errs.toString().replace("{aa}", scope.aa);
	                	
	                	_errs = errs ;
	                	if(_errs!=''){
		        			scope.$parent.warningMsg.conditionMessage = _errs;
		        		}
		        		keepGoing = false;
	                }
	               
        		})
        		
        		
        		return value;
            }

            ctrl.$parsers.push(myValidation);
        }
    };
}]);
