angular.module('starter.controllers', [])

.controller('LoginCtrl', function($scope, $http, $location, $state) {
    $scope.login = function(){
      $.jStorage.set('kode', $scope.kode);
      $.jStorage.set('ea', $scope.ea);
     $state.go("survey.dash");
    }
})
.controller('DashCtrl', function($scope, $http, $location, $state) {
    
})
.controller('QuestCtrl', function($scope, $state, $stateParams, Quest, $q, $http,$rootScope,DB, $ionicPopup, $compile, Answer) {


    $scope.data = {};
    $scope.id_buku = $stateParams.id_buku;
    $scope.key_header = $stateParams.id_header;
    $scope.id_quest = $stateParams.id_quest;
    $scope.id_roster = $stateParams.id_roster;
    $scope.answer  = '';
    $scope.crosstab = {};
    
    $scope.hint  = '';
    $scope.roster  = '';
    $scope.isinsert = false;
    $scope.insertOther = false;
    $scope.id_survey = $stateParams.id_survey;
    $scope.kode_petugas = $.jStorage.get('kode');
    $scope.surveyForm = {};
    $scope.is_valid = true;
    
    $scope.init = function(){
        if(typeof $scope.id_buku!=''){

            Quest.data($scope.id_buku).then(function(result){
                
                $scope.data = result.data;
                $scope.header = $scope.data[$scope.key_header];
                if(typeof $scope.header.crosstab!='undefined'){
                    initCrosstab = Object.keys($scope.header.crosstab).length;
                }

                $scope.getRoster( );
                
                if($scope.header.is_crosstab == true && $scope.header.is_custom==true){

                    $scope.insertOther = true;
                }
                $scope.id_header = $scope.header.id;
                
                $scope.quest = $scope.data[$scope.key_header].quest[$scope.id_quest];

                if(typeof $scope.header.crosstab != 'undefined' && $state.current.name=="survey.quests" && typeof $scope.header.crosstab[$scope.id_roster] !='undefined'){

                    $scope.quest.label = $scope.quest.label.toString().replace("{}",$scope.header.crosstab[$scope.id_roster].nama);
                    
                }
                
                
                $scope.getHint = function(){
                    if(typeof $scope.quest!='undefined'){
                        Quest.getHint( $scope.id_survey, $scope.quest.hint).then(function(result){
                           
                            var dataHint = $scope.header.quest[$scope.quest.hint];

                            angular.forEach(result, function(value,key){
                                
                                if(dataHint.content[value.key].type=="checkbox"){
                                      $scope.hint = dataHint.content[value.key].label;
                                }else if(dataHint.content[value.key].type=="text"){
                                      $scope.hint = value.value;
                                }else if(dataHint.content[value.key].type=="other"){
                                    
                                      $scope.hint = dataHint.content[value.key].label +' '+value.value_2;
                                }
                                if($scope.quest.hint){
                                    $scope.quest.label = $scope.quest.label.toString().replace("{{hint}}",$scope.hint);
                                }
                                
                            })
                            
       
                        });
                    }
                }

                $scope.getHint();

                if($scope.header.last && $scope.quest.next_to =='finish'){
                    $scope.end = true;
                }else{
                     $scope.end = false;
                }
                
               
                if(typeof $scope.quest!='undefined'){
                    
                    if($scope.header.next!=''&&$scope.quest.last==true){
                         
                        $scope.nextHeader = $scope.header.next;
                       
                        $scope.nextQuest = Object.keys($scope.data[$scope.header.next].quest)[0];
                       
                    }

                    if($scope.quest.next>0&&$scope.header.next!=''){
                        $scope.nextQuest = $scope.quest.next;
                        $scope.nextHeader = $scope.key_header;
                        
                    }

                    $scope.is_roster($scope.nextHeader);
                     
                    if($scope.quest.next_to == 'finish' || $scope.quest.last){


                        $scope.finish();   

                    }
                     

                }
                
                if($state.current.name=="survey.roster"){
                    $scope.nextHeader = $scope.header.next;
                    $scope.is_roster($scope.nextHeader);
                    $scope.nextQuest = Object.keys($scope.data[$scope.header.next].quest)[0];
                }
                
                angular.forEach($scope.quest.content, function(value,key){

                    Answer.getByIdQuestion($scope.id_survey, $scope.id_header, $scope.id_quest, key, $scope.id_roster, $scope.quest).then(function(result){

                        if(typeof result !='undefined'){
                            if(value.type=="checkbox"){
                                $scope.quest.content[result.key].value = (result.value === 'true');
                               
                            }
                            else if(value.type=="other"){
                                
                                $scope.quest.content[result.key].value = (result.value === 'true');                  
                                $scope.quest.content[result.key].value_2 = result.value_2;                                   
                                $scope.quest.content[result.key].value_3 = result.value_3;                                   
                            }else{
                                 
                                if(value.replicate > 0){

                                    value.value = [];
                                    value.value_2 = [];
                                    
                                    angular.forEach(result, function(value, key){
                                        $scope.quest.content[value.key].value[value.key_multiple] = value.value;                             
                                        $scope.quest.content[value.key].value_2[value.key_multiple] = value.value_2;                             
                                    })

                                    
                                }else{
                                    if(typeof $scope.quest.content[result.key]!='undefined'){
                                        if( result.value_2!="" && typeof  result.value_2!="undefined"){
                                            $scope.quest.content[result.key].value_2 = result.value_2;   
                                        }
                                        $scope.quest.content[result.key].value = result.value;  
                                    }
                                        
                                        
                                }
                                
                            }
                        }
                    });
                });

            });
            
            Quest.getHistory( $scope).then(function(result){

                if(typeof result!='undefined'){
                    $scope.prevHeader = result.id_header;
                    $scope.prevQuest = result.id_quest;
                    $scope.check_roster_before(result.id_header);
                }
                
            });

            
        }
        
    }
    $scope.check_roster = function(){
        
        if($scope.header.crosstab){
            $scope.is_valid = false;
        }
        angular.forEach($scope.header.crosstab, function(value, key){
            if(value.status=="0" || value.status=="1"){
                $scope.is_valid = false;
                return false;
            }else{
                $scope.is_valid = true;
            }
        });
        
    }
    $scope.getRoster = function(){

       
        Quest.getRoster($scope.id_survey,$scope.id_buku, $scope.key_header).then(function(result){
            
            if(typeof $scope.header.crosstab!='undefined'){
                
                angular.forEach(result, function(value, key){
                    
                        if($scope.header.is_empty==false){
                            key = Object.keys($scope.header.crosstab)[Object.keys($scope.header.crosstab).length - 1];
                            p_key = parseInt(key) + 1;
                            $scope.header.crosstab[p_key.toString()] = {"nama":value.value, "code":value.code};
                        }else{
                            $scope.header.crosstab[value.code.toString()] = {"nama":value.value, "code":value.code, "status":value.status};
                        }
                        
                    })
                if(typeof $scope.header.crosstab != 'undefined' && $state.current.name=="survey.quests" && typeof $scope.header.crosstab[$scope.id_roster] !='undefined'){
                        
                        $scope.quest.label = $scope.quest.label.toString().replace("{}",$scope.header.crosstab[$scope.id_roster].nama);
                    }
                if($scope.header.is_crosstab == true && Object.keys($scope.header.crosstab).length > 0 && $scope.header.is_custom == true && result.length > 0){
                     $scope.insertOther = false;
                }
                if($scope.header.is_empty){
                    $scope.insertOther = true;
                }
            }
             $scope.check_roster();
        })

       
    }
    $scope.saveRoster = function(){
        var status = 0;
       if($scope.header.is_empty==true){
            code = Object.keys($scope.header.crosstab).length + 1;
            p_key = code;
            DB.query("SELECT COUNT(*) FROM ms_crosstab WHERE id_survey = ? AND id_header = ? AND id_survey = ?");
            if(code==1 ){
                status = 1;
                status = status.toString();
            }else{
                status = 0;
                status = status.toString();
            }

        }
        else{

            DB.query("DELETE FROM ms_crosstab WHERE id_survey=? AND key_header = ? AND id_buku = ?",[$scope.id_survey,$scope.key_header, $scope.id_buku]);
        
            code = 'v';
            key = Object.keys($scope.header.crosstab)[Object.keys($scope.header.crosstab).length - 1];
            
            p_key = parseInt(key) + 1;
            
        }

        query = DB.query("INSERT INTO ms_crosstab(id_survey, kode_petugas, id_buku, key_header, id_header, value,code,status, entry_stamp) VALUES(?,?,?,?,?,?,?,?,?)",
                    [$scope.id_survey, $scope.kode_petugas, $scope.id_buku, $scope.key_header,$scope.id_header, $scope.crosstab.value.toUpperCase(), code.toString().toUpperCase(),status, moment().format('YYYY-MM-DD HH:mm:ss')]
                    );
        
        
        $scope.header.crosstab[p_key] = {"nama":$scope.crosstab.value.toUpperCase(), "code":code,"status":status};
        if($scope.header.is_crosstab == true && Object.keys($scope.header.crosstab).length > 0 && $scope.header.is_custom == true){
            $scope.insertOther = false;
        } 
        
        if($scope.header.is_crosstab == true && $scope.header.is_empty==true && $scope.header.is_custom == true){
            $scope.insertOther = true;
            $scope.saveOther = false;
            $scope.crosstab.value = '';
        }
        
        $scope.displayForm = false;
        $scope.check_roster();
    }

    $scope.create_remark = function(key,data){
        $scope._data = {};
          var myPopup = $ionicPopup.show({
            template: '<textarea ng-model="_data.remark" rows="5"></textarea>',
            title: 'Catatan Pewawancara',
            scope: $scope,
            buttons: [
              { text: 'Cancel' },
              {
                text: '<b>Save</b>',
                type: 'button-positive',
                onTap: function(e) {

                    query = DB.query('INSERT INTO ms_remark(id_survey,id_buku,  id_quest, variable, remark) VALUES (?,?,?,?,?)',[$scope.id_survey, $scope.id_buku, $scope.id_quest, $scope.quest.code,$scope._data.remark ]);
                    
                }
              }
            ]
          });
    }
    $scope.edit_roster = function(key,data){
        $scope._data = {};
          var myPopup = $ionicPopup.show({
            template: '<input type="text" ng-model="_data.newValue">',
            title: 'Edit',
            scope: $scope,
            buttons: [
              { text: 'Cancel' },
              {
                text: '<b>Save</b>',
                type: 'button-positive',
                onTap: function(e) {
                    data.nama = $scope._data.newValue;
                    query = DB.query('UPDATE ms_crosstab SET value = ? WHERE code = ? AND id_survey = ?',[$scope._data.newValue, data.code, $scope.id_survey]);

                }
              }
            ]
          });
    }
    $scope.delete_roster = function(key,data){
        $scope._data = {};
          var myPopup = $ionicPopup.show({
            title: 'Apakah benar akan menghapus data?',
            scope: $scope,
            buttons: [
              { text: 'Cancel' },
              {
                text: '<b>Hapus</b>',
                type: 'button-assertive',
                onTap: function(e) {
                    console.log($scope)
                    query = DB.query('DELETE FROM ms_crosstab WHERE code = ? AND id_survey = ?',[data.code, $scope.id_survey]);
                     delete $scope.header.crosstab[key];
                }
              }
            ]
          });
    }

    $scope.insert = function(){
        $scope.displayForm = true;
        $scope.insertOther = false;
        $scope.saveOther = true;
    }
    $scope.is_roster = function(next){
        if(typeof $scope.data[next]!= 'undefined'){
           
            if($scope.data[next].is_crosstab==true && $scope.id_roster == ''){

                $scope.roster = true;
            }else{
                $scope.roster = false;
            }
        }
         
    }

    $scope.check_roster_before = function(before){

        if(typeof $scope.data[before]!= 'undefined'){
            if($scope.data[before].is_crosstab==true){
                $scope.is_roster_before = true;
            }else{
                $scope.is_roster_before = false;
            }
        }
        
    }

    $scope.saveParsial = function(){
        if( $scope.id_roster!=''){
            query = DB.query('UPDATE ms_master SET id_last_header = ?, id_key_header = ?, id_last_quest =? WHERE id = ?',
                [$scope.id_header, $scope.key_header, $scope.id_quest, $scope.id_survey]);
        }else{
            query = DB.query('UPDATE ms_master SET id_last_header = ?, id_key_header = ?, id_last_quest =?, id_last_roster=? WHERE id = ?',
                [$scope.id_header, $scope.key_header, $scope.id_quest, $scope.id_roster, $scope.id_survey]);
        }
        
        $state.go("survey.start", {
            id_buku: $scope.id_buku
        });
    }
    $scope.finish = function(){
        if($scope.quest.last){
            $scope.is_roster($scope.header.next);
        }
        $scope.nextHeader = $scope.header.next;
        $scope.nextQuest = Object.keys($scope.data[$scope.header.next].quest)[0];
    }
    $scope.save = function(){
        
        if(typeof $scope.id_roster == 'undefined' || $scope.id_roster =='' || $scope.is_crosstab ==false ){
            query = DB.query("DELETE FROM ms_result WHERE id_survey=? AND key_header = ? AND id_header = ? AND id_quest = ?",[$scope.id_survey,$scope.key_header, $scope.id_header, $scope.id_quest]);
            
            angular.forEach($scope.quest.content, function(value, key){

              
                if(value.replicate>0){
                    
                    angular.forEach(value.value, function(value_multiple, key_multiple){

                        query = DB.query("INSERT INTO ms_result(id_survey, kode_petugas, id_buku, key_header, id_header,id_quest,key, key_multiple, id_crosstab,value,value_2, entry_stamp) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)",
                        [$scope.id_survey, $scope.kode_petugas, $scope.id_buku, $scope.key_header,$scope.id_header, $scope.id_quest, key, key_multiple, null, value_multiple, value.value_2[key_multiple], moment().format('YYYY-MM-DD HH:mm:ss')]
                        );
                        
                    });
                }else{

                    if(((value.value==true ||value.value=='true') && value.type=="checkbox")||(value.type=="text" && value.type!='')){
                        query = DB.query("INSERT INTO ms_result(id_survey, kode_petugas, id_buku, key_header, id_header,id_quest,key,id_crosstab,value,value_2,  entry_stamp) VALUES(?,?,?,?,?,?,?,?,?,?,?)",
                        [$scope.id_survey, $scope.kode_petugas, $scope.id_buku, $scope.key_header,$scope.id_header, $scope.id_quest, key, null, value.value, value.value_2, moment().format('YYYY-MM-DD HH:mm:ss')]
                        );
                        
                    }
                    if(value.type=="other" && value.type!='' && value.value==true){

                        query =  DB.query("INSERT INTO ms_result(id_survey, kode_petugas, id_buku, key_header, id_header,id_quest,key,id_crosstab,value,value_2, value_3, entry_stamp) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)",
                        [$scope.id_survey, $scope.kode_petugas, $scope.id_buku, $scope.key_header,$scope.id_header, $scope.id_quest, key, null, value.value, value.value_2, value.value_3, moment().format('YYYY-MM-DD HH:mm:ss')]
                        );
                    }

                    
                }

                
            });
        }else{

            query = DB.query("DELETE FROM ms_result WHERE id_survey=? AND key_header = ? AND id_header = ? AND id_quest = ? AND id_crosstab = ?",[$scope.id_survey,$scope.key_header, $scope.id_header, $scope.id_quest, $scope.id_roster]);
            angular.forEach($scope.quest.content, function(value, key){

                if(((value.value==true ||value.value=='true') && value.type=="checkbox")||(value.type=="text" && value.type!='')){
                    query = DB.query("INSERT INTO ms_result(id_survey, kode_petugas, id_buku, key_header, id_header,id_quest,key,id_crosstab,value, entry_stamp) VALUES(?,?,?,?,?,?,?,?,?,?)",
                    [$scope.id_survey, $scope.kode_petugas, $scope.id_buku, $scope.key_header,$scope.id_header, $scope.id_quest, key, $scope.id_roster, value.value, moment().format('YYYY-MM-DD HH:mm:ss')]
                    );
                }

                if(value.type=="other" && value.type!='' && value.value==true){
                    query =  DB.query("INSERT INTO ms_result(id_survey, kode_petugas, id_buku, key_header, id_header,id_quest,key,id_crosstab,value,value_2,  entry_stamp) VALUES(?,?,?,?,?,?,?,?,?,?,?)",
                    [$scope.id_survey, $scope.kode_petugas, $scope.id_buku, $scope.key_header,$scope.id_header, $scope.id_quest, key, $scope.id_roster, value.value, value.value_2, moment().format('YYYY-MM-DD HH:mm:ss')]
                    );
                }
                if($scope.quest.content[key].next_to=='finish'){
                    
                    query = DB.query("UPDATE ms_crosstab SET status = ? WHERE id_survey=? AND code = ? AND id_buku = ? AND id_header = ?",["2", $scope.id_survey,$scope.id_roster, $scope.id_buku, $scope.id_header]);
                  
                    next = DB.query("UPDATE ms_crosstab SET status = ? WHERE id_survey=? AND code = ? AND id_buku = ? AND id_header = ?",["1", $scope.id_survey,(parseInt($scope.id_roster) + 1).toString(), $scope.id_buku, $scope.id_header]);
                    
                }  
            });

        } 
        
        if($scope.header.last && $scope.quest.next_to =='finish'){
            $scope.end = true;
        }else{
             $scope.end = false;
        }

        Quest.saveHistory($scope);

    }
    $scope.init();
    $scope.remove = function(chat) {
        Quest.remove(chat);
    };
})

.controller('MasterCtrl', function($scope, $stateParams, DB, $state, Master, Book, $q, $http,$rootScope,$ionicPopup, Quest) {

    $scope.survey = {};
    $scope.id_buku = $stateParams.id_buku;
    $scope.id_survey = $stateParams.id_survey;
    $scope.kode_petugas = $.jStorage.get('kode');
    $scope.kode_ea = $.jStorage.get('ea');
    $scope.data_survey = {
        no : '',
        id_art : '',
        nama : '',
        spv : '',
        dusun : '',
        rw : '',
        rt: '',
        address: '',
        location:'',
        is_telp:false,
        phone:'',
        is_hp:false,
        hp:'',
        tt:false
    };

    Master.all($scope.id_buku, $scope.kode_ea).then(function(result){

        $scope.survey = result;

    });
    Master.getById($scope.id_survey).then(function(result){
        $scope.data_survey.no = result.no;
        $scope.data_survey.id_art = result.id_art;
        $scope.data_survey.nama = result.nama;
        $scope.data_survey.spv = result.spv;
        $scope.data_survey.dusun = result.dusun;
        $scope.data_survey.rw = result.rw;
        $scope.data_survey.rt = result.rt;
        $scope.data_survey.address = result.address;
        $scope.data_survey.location = result.location;
        $scope.data_survey.is_telp = result.is_telp;
        $scope.data_survey.phone = result.phone;
        $scope.data_survey.is_hp = result.is_hp;
        $scope.data_survey.hp = result.hp;
        $scope.data_survey.tt = result.tt;

      
    });
     Master.getEa().then(function(response){
        $scope.ea = response.data[$scope.kode_ea];
    })
    Quest.data($scope.id_buku).then(function(result){
        $scope.q = result.data;

    });
    Book.data.then(function(response){
        $scope.buku = response.data;
    })
    
    $scope.emptyPhone = function(data){
        data.phone = '';
        data.hp = '';
        data.owner = '';
        data.is_hp = false;
        data.is_telp = false;
    }
    $scope.emptyTT = function(data){
        data.tt = '';
    }
    $scope.saveConsent = function(data){
        query = DB.query("INSERT INTO ms_master(id_buku,id_enumerator, id_ea, tt, answer,remark,entry_stamp) VALUES(?,?,?,?,?,?,?)",
            [$scope.id_buku, $scope.kode_petugas, $scope.kode_ea, data.tt, data.answer, data.remark,  moment().format('YYYY-MM-DD HH:mm:ss')]
        ).then(function(result){

            if(data.answer==1){
                $state.go("survey.master", {
                    id_survey: result.insertId,
                    id_buku: $scope.id_buku
                });
            }else{
                $state.go("survey.remark", {
                    id_survey: result.insertId,
                    id_buku: $scope.id_buku
                });
            }
        }); 
    }
    $scope.save = function(data){
        var kode_buku = "";
        switch($scope.id_buku){
            case '4':
            case '5':
            case '2':
            case '8':
                kode_buku = $scope.buku[$scope.id_buku].code + $scope.kode_ea;
                break;
            case '7':
            case '6':
            case '1':
            case '3':
                kode_buku = $scope.buku[$scope.id_buku].code + $scope.kode_ea + data.no;
                break;
            case '9':
                kode_buku = $scope.buku[$scope.id_buku].code + $scope.kode_ea + data.no + data.id_art;
                break;

        }
        query = DB.query("UPDATE ms_master SET no =?, id_art=?, kode_buku=?, nama=?, spv=?, dusun=?, rw=?, rt=?, address=?, location=?, phone=?, hp=?, owner=?, is_telp=?, is_hp=?, tt=? WHERE id = ?",
                            [data.no, data.id_art, kode_buku, data.nama, data.spv, data.dusun, data.rw, data.rt, data.address, data.location, data.phone, data.hp, data.owner,data.is_telp, data.is_hp,data.tt, $stateParams.id_survey])
        .then(function(result){
                var first_header = Object.keys($scope.q)[0];

                if($scope.q[first_header].is_crosstab){
                    $state.go("survey.roster", {
                        id_buku: $scope.id_buku,
                        id_survey: $stateParams.id_survey,
                        id_header: 1,
                        id_quest: Object.keys($scope.q[first_header].quest)[0]
                    });
                }else{
                    $state.go("survey.quests", {
                        id_buku: $scope.id_buku,
                        id_survey: $stateParams.id_survey,
                        id_header: 1,
                        id_quest: Object.keys($scope.q[first_header].quest)[0]
                    });
                }
        }); 
        
    }
})
.controller('RemarkCtrl', function($scope, $stateParams, DB, $state, Master, Book) {
    $scope.id_survey = $stateParams.id_survey;
    $scope.id_buku = $stateParams.id_buku;
    $scope.insertOther = true;
    Master.remark($scope.id_survey, $scope.id_buku).then(function(result){
        $scope.remark = result;
    });
    $scope.insert = function(){
        $scope.displayForm = true;
        $scope.insertOther = false;
        $scope.saveOther = true;
    }
    $scope.kode_petugas = $.jStorage.get('kode');
    $scope.save = function(data){
        console.log(data.variable.toString().toUpperCase());
        query = DB.query("INSERT INTO ms_remark(id_survey, id_buku, variable,remark,entry_stamp,edit_stamp) VALUES(?,?,?,?,?,?)",
            [$scope.id_survey, $scope.id_buku,  data.variable.toString().toUpperCase(), data.remark,  moment().format('YYYY-MM-DD HH:mm:ss'),null]
        ).then(function(result){
            code = Object.keys($scope.remark).length;
            p_key = code;
            $scope.remark[p_key] = {"variable":data.variable.toString().toUpperCase(), "remark":data.remark};


        });
        console.log(query);
        
       
    }

})
.controller('BukuCtrl', function($scope, Book, $stateParams, DB, $state) {
    $scope.id_survey = $stateParams.id_survey;
    $scope.buku = {};
    Book.data.then(function(response){
        $scope.buku = response.data;
    })
});
